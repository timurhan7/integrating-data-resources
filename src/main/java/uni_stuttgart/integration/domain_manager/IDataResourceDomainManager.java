package uni_stuttgart.integration.domain_manager;

import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;

import uni_stuttgart.ipsm.protocols.integration.Deployable;
import de.uni_stuttgart.iaas.ipsm.v0.TIntention;

/**
 * This interface needs to be implemented to add a new data source.
 * The data source specific code for listing the data is hidden behind this interface.
 * @author wederbn
 *
 */
public interface IDataResourceDomainManager {
    
    /**
     * Access the data source and create TOSCA TEntityTemplates (e.g. TNodeType)
     * for the data resources.
     *
     * @return list of all resources as TEntityTemplates
     */
    List<TExtensibleElements> listEntities();

    /**
     * Return current imports
     *
     * @return list of all imports that are used in a TEntityTemplate of this
     *         data source
     */
    List<ImportObject> getImports();

    /**
     * Return the QName of every resource that is supported by this
     * DataResourceDomainManager
     *
     * @return list of all QNames
     */
    List<QName> getSupportedQNames();
    
    /**
     * Creates and returns one of the possible deployables
     * of the desired resource. Which deployable is to be returned
     * is chosen by the second QName.
     * 
     * @param resourceQname The QName of the resource.
     * @param deployableType The QName of the desired deployable.
     * @param intentionInformation The intention for the resource.
     * @return The chosen Deployable for the resource.
     */
    Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention);

    /**
     * List types of available deployables for a specific resource type
     * @param
     * resourceType is the type of resource provided by this domain manager
     * @return List of the QNames representing different type of
     * deployables provided for this resource type actual deployables
     * are provided by getDeployables
     */
    List<QName> listDeployablesOfResource(QName resourceType);
}
