package uni_stuttgart.integration.domain_manager.implementations.knowledge_resources;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TImport;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;
import org.oasis_open.docs.tosca.ns._2011._12.TRelationshipType;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType.PropertiesDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.PullImageResultCallback;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.DeployableObject;
import uni_stuttgart.integration.domain_manager.IDataResourceDomainManager;
import uni_stuttgart.integration.domain_manager.ImportObject;
import uni_stuttgart.integration.domain_manager.XsdCreator;
import uni_stuttgart.integration.meta_data_objects.DockerMetaData;
import uni_stuttgart.ipsm.protocols.integration.Deployable;

/**
 * Class for usage of Mediawiki servers as data sources. Uses docker clients to 
 * retrieve the different active Mediawiki servers in docker containers of the 
 * related docker daemons. As docker image the synctree/mediawiki image is used.
 * As database image the appcontainers/mysql image is used. Different tags of
 * this image are used to store and recreate mediawiki instances.
 * 
 * The class also lists the different possible AdminRelationship relationships.
 *
 * @author wederbn
 */
@Service
public class MediawikiResourceDomainManager implements IDataResourceDomainManager{
    
    // list to store current Imports
    private ArrayList<ImportObject> importList = new ArrayList<ImportObject>();
    // list to store current QNames
    private ArrayList<QName> nameList = new ArrayList<QName>();
    // context to load the ConfigurationProvider
    private AnnotationConfigApplicationContext context = null;
    // the namespace of the definitions documents that can be created by the aggregator this DM works for
    private String namespace;
    // list to store the meta-data for all available docker clients
    private ArrayList<DockerMetaData> dataSources = new ArrayList<DockerMetaData>();
    
    private String getNamespace() {
        return this.namespace;
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }

    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }
    
    public MediawikiResourceDomainManager(){
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);
        
        // read meta data from properties file
        this.setNamespace(provider.getPropertyString("definitions.namespace"));
        String[] urls = provider.getPropertyStringArray("Docker.uri");
        String[] certPaths = provider.getPropertyStringArray("Docker.certs");
        
        // calculate number of correct inserted GitRepositoryMetaDatas
        int dockerClientNumber = Math.min(certPaths.length, urls.length);
        
        // save meta-data of the docker clients
        this.dataSources = new ArrayList<DockerMetaData>();
        for (int i = 0; i < dockerClientNumber; i++) {
            this.dataSources.add(new DockerMetaData(urls[i], certPaths[i]));
        }
    }

    public List<TExtensibleElements> listEntities() {
        // clear lists for new request
        this.importList.clear();
        this.nameList.clear();
        ArrayList<TExtensibleElements> entityList = new ArrayList<TExtensibleElements>();
        
        // create TImport for usage in the definitions document
        TImport toscaImport = new TImport();
        toscaImport.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport.setNamespace(this.getNamespace() + "resources/knowledge-resources/xsd/mediawiki");
        entityList.add(toscaImport);
        
        TImport toscaImport2 = new TImport();
        toscaImport2.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport2.setNamespace(this.getNamespace() + "relationships/xsd/mediawiki/adminRelationship");
        entityList.add(toscaImport2);
        
        try {
            // create Import to describe the properties of the NodeTypes of this DM
            List<String> attributes = new LinkedList<String>();
            attributes.add("mediawikiName");
            attributes.add("mediawikiAdmin");
            attributes.add("mediawikiAdminPass");
            InputStream stream = XsdCreator.createXSD(this.getNamespace() + "resources/knowledge-resources/xsd/mediawiki", "MediawikiProperties", attributes);
            ImportObject dmImport = new ImportObject(new URI(this.getNamespace() + "resources/knowledge-resources/xsd/mediawiki"), new URI("http://www.w3.org/2001/XMLSchema"), stream);
            importList.add(dmImport);
            
            // create Import to describe the properties of the RelationshipTypes of this DM
            attributes = new LinkedList<String>();
            attributes.add("userName");
            attributes.add("password");
            InputStream stream2 = XsdCreator.createXSD(this.getNamespace() + "relationships/xsd/mediawiki/adminRelationship", "AdminRelationshipProperties", attributes);
            ImportObject dmImport2 = new ImportObject(new URI(this.getNamespace() + "relationships/xsd/mediawiki/adminRelationship"), new URI("http://www.w3.org/2001/XMLSchema"), stream2);
            importList.add(dmImport2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // iterate over every docker meta-data set
        for (DockerMetaData currentSource : this.dataSources) {
            // create docker client
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(currentSource.getURI()).withDockerCertPath(currentSource.getCertPath());
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            String uri = currentSource.getURI().substring(6); //cut of tcp://
            
            // pull the needed Image if not already done
            dockerClient.pullImageCmd("appcontainers/mysql:latest").exec(new PullImageResultCallback()).awaitSuccess();
            dockerClient.pullImageCmd("synctree/mediawiki:latest").exec(new PullImageResultCallback()).awaitSuccess();
            
            TNodeType element;
            // define mediawiki properties
            PropertiesDefinition prop = new PropertiesDefinition();
            prop.setElement(new QName(this.getNamespace() + "resources/knowledge-resources/xsd/mediawiki", "MediawikiProperties"));
            
            //iterate over all available images and add a empty resource for each
            List<Image> images = dockerClient.listImagesCmd().exec();
            for(Image image : images){
                for(String tag : image.getRepoTags()){
                    if(tag.startsWith("appcontainers/mysql")){
                        String tagName;
                        if(tag.equals("appcontainers/mysql:latest")){
                            // new unchanged MediaWiki
                            tagName = "newMediawiki";
                        }else{
                            // saved changed MediaWiki
                            tagName = tag.split(":")[1];
                        }
                        // add a empty resource for every docker client
                        element = new TNodeType();
                        element.setTargetNamespace(this.getNamespace() + "resources/knowledge-resources/mediawiki");
                        element.setName(uri + "/" + tagName + "/" + "Empty Resource");
                        
                        // add properties element
                        element.setPropertiesDefinition(prop);
                        
                        entityList.add(element);
                        this.nameList.add(new QName(this.getNamespace() + "resources/knowledge-resources/mediawiki", uri + "/" + tagName + "/" + "Empty Resource"));      
                    }
                }
            }   
            
            // iterate over all containers in the docker client
            List<Container> containers = dockerClient.listContainersCmd().exec();
            for(Container container : containers){
                if(container.getImage().startsWith("synctree/mediawiki")){
                    // add a resource for every container with the synctree/mediawiki image
                    element = new TNodeType();
                    element.setTargetNamespace(this.getNamespace() + "resource-instances/knowledge-resources/mediawiki");
                    element.setName(uri + "/" + container.getNames()[0].substring(1)); // read name and cut of the slash at the beginning
                    // add properties element
                    element.setPropertiesDefinition(prop);
                    entityList.add(element);
                    this.nameList.add(new QName(this.getNamespace() + "resource-instances/knowledge-resources/mediawiki", uri + "/" + container.getNames()[0].substring(1)));
                    
                    //TODO: transfer relationship to separate DM?
                    // add a admin relationship for every container with the synctree/mediawiki image
                    TRelationshipType relationship = new TRelationshipType();
                    relationship.setTargetNamespace(this.getNamespace() + "relationships/mediawiki/adminRelationship");
                    relationship.setName(uri + "/" + container.getNames()[0].substring(1));
                    // add properties element
                    PropertiesDefinition propRelationship = new PropertiesDefinition();
                    propRelationship.setElement(new QName(this.getNamespace() + "relationships/xsd/mediawiki/adminRelationship", "AdminRelationshipProperties"));
                    relationship.setPropertiesDefinition(propRelationship);
                    entityList.add(relationship);
                    this.nameList.add(new QName(this.getNamespace() + "relationships/mediawiki/adminRelationship" , uri + "/" + container.getNames()[0].substring(1)));
                }
            }
        }
        return entityList;
    }
    
    public Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention) {
        // resourceQname and deployableType need to be the same for mysql
        if(!this.nameList.contains(resourceQname) || !resourceQname.equals(deployableType)){
            return null;
        }
        
        // get the information needed to access the data of the resource represented by the QName
        Deployable depl = null;
        String loadInformation = createDeployableStringForRequestedResource(resourceQname);
        
        try {
            if(resourceQname.getNamespaceURI().equals(this.getNamespace() + "relationships/mediawiki/adminRelationship")){
                // create deployable for the admin relationship
                InputStream stream = IOUtils.toInputStream(loadInformation, "UTF-8");
                depl = new DeployableObject(stream, deployableType);
            }else{
                if(resourceQname.getLocalPart().endsWith("Empty Resource")){
                    // create deployable for a docker image
                    InputStream stream = IOUtils.toInputStream(" |" + loadInformation, "UTF-8");
                    depl = new DeployableObject(stream, deployableType);
                }else{
                    // create a deployable for a active docker instance
                    InputStream stream = IOUtils.toInputStream(loadInformation + "| ", "UTF-8");
                    depl = new DeployableObject(stream, deployableType);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return depl;
    }

    public List<QName> listDeployablesOfResource(QName resourceType) {
        List<QName> nameList = new ArrayList<QName>();
        // for mediawiki instances just one store format exists (docker image <--> active instance)
        nameList.add(resourceType);
        return nameList;
    }

    /**
     * Create a String with all needed information to access the resource represented by the QName.
     * 
     * @param type The QName of the resource.
     * @return A String with the needed information.
     */
    private String createDeployableStringForRequestedResource(QName type) {
        // check if QName is supported by this DM
        if (this.nameList.contains(type)) {
            String uri = "tcp://" + type.getLocalPart().split("/")[0]; // get docker uri
            
            // search QName related Docker Client
            for (DockerMetaData currentSource : this.dataSources) {
                if (uri.equals(currentSource.getURI())) {
                    // add information for accessing the resource
                    String resourceEngager;
                    if(type.getNamespaceURI().equals(this.getNamespace() + "relationships/mediawiki/adminRelationship")){
                        resourceEngager = this.getNamespace() + "resources/knowledge-resources/mediawiki" + ";";
                    }else{
                       resourceEngager = type.getNamespaceURI() + ";"; 
                    }
                    resourceEngager += type.getLocalPart() + ";";
                    resourceEngager += currentSource.getURI() + ";";
                    resourceEngager += currentSource.getCertPath() + ";";
                    return resourceEngager;
                }
            }
        }
        // return null if QName is not supported or an error occurred
        return null;
    }

    public List<ImportObject> getImports() {
        // return List of Imports from last listEntities() call
        return this.importList;
    }

    public List<QName> getSupportedQNames() {
        // getSupportedQNames() requests depend on last listEntities() call
        return this.nameList;
    }
}
