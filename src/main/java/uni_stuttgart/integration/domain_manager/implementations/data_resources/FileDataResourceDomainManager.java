package uni_stuttgart.integration.domain_manager.implementations.data_resources;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType.PropertiesDefinition;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TImport;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.DeployableObject;
import uni_stuttgart.integration.domain_manager.IDataResourceDomainManager;
import uni_stuttgart.integration.domain_manager.ImportObject;
import uni_stuttgart.integration.domain_manager.XsdCreator;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import de.uni_stuttgart.iaas.ipsm.v0.TIntention;

/**
 * Class for usage of local folders as data sources. 
 * Uses the local file system to access data.
 * 
 * @author wederbn
 */
@Service
public class FileDataResourceDomainManager implements IDataResourceDomainManager{

    // list to store current Imports
    private ArrayList<ImportObject> importList = new ArrayList<ImportObject>();
    // list to store current QNames
    private ArrayList<QName> nameList = new ArrayList<QName>();
    // context to load the ConfigurationProvider
    private AnnotationConfigApplicationContext context = null;
    // the namespace of the definitions documents that can be created by the aggregator this DM works for
    private String namespace;
    // list to store all source files for file data resources
    private ArrayList<String> dataSources = new ArrayList<String>();
    // list of all alternatives to store the data from this DM
    private ArrayList<String> deployableAlternatives = new ArrayList<String>();
    // list of all QNames of alternatives to store the data from this DM
    private ArrayList<QName> deployableAlternativesQNames = new ArrayList<QName>();


    private String getNamespace() {
        return this.namespace;
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }

    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }

    public FileDataResourceDomainManager() {
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);

        // read meta data from properties file
        String[] sourcePaths = provider.getPropertyStringArray("FileDataResourceDomainManager.sourcePaths");
        this.setNamespace(provider.getPropertyString("definitions.namespace"));

        // add source paths to the list
        for (String path : sourcePaths) {
            this.dataSources.add(path);
        }
        
        // add deployable alternatives
        // alternative to store data in a git repo
        String resourceEngager = this.getNamespace() + "resources/data-resources/git/repository" + ";";
        resourceEngager += "Integrating-Test/Empty Resource" + ";";
        resourceEngager += "Integrating-Test" + ";";
        resourceEngager += "testtest1" + ";";
        deployableAlternatives.add(resourceEngager);
        deployableAlternativesQNames.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", "Integrating-Test/Empty Resource"));
        // alternative to store data in dropbox
        resourceEngager = this.getNamespace() + "resources/data-resources/dropbox" + ";";
        resourceEngager += "integrating.dataresources@web.de/Empty Resource" + ";";
        resourceEngager += "qXPJQq4y8JAAAAAAAAAACwNE3f7Z8Q9lT686lVFmQ9cuQT61nHrbWdpaCE7HuUie" + ";";
        resourceEngager += "integrating.dataresources@web.de" + ";";
        resourceEngager += "testtesttest" + ";";
        deployableAlternatives.add(resourceEngager);
        deployableAlternativesQNames.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", "integrating.dataresources@web.de/Empty Resource"));
    }
    
    public List<TExtensibleElements> listEntities() {
        // clear lists for new request
        this.importList.clear();
        this.nameList.clear();
        ArrayList<TExtensibleElements> entityList = new ArrayList<TExtensibleElements>();
        
        // create TImport for usage in the definitions document
        TImport toscaImport = new TImport();
        toscaImport.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport.setNamespace(this.getNamespace() + "resources/data-resources/xsd/file");
        entityList.add(toscaImport);
        try {
            // create Import to describe the properties of the NodeTypes of this DM
            List<String> attributes = new LinkedList<String>();
            attributes.add("name");
            InputStream stream = XsdCreator.createXSD(this.getNamespace() + "resources/data-resources/xsd/file", "EmptyResourceProperties", attributes);
            ImportObject dmImport = new ImportObject(new URI(this.getNamespace() + "resources/data-resources/xsd/file"), new URI("http://www.w3.org/2001/XMLSchema"), stream);
            importList.add(dmImport);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // iterate over every data source
        for (String currentPath : this.dataSources) {
            File file = new File(currentPath);
            // read local parts of every data resource and transform global path to namespace consistent form
            if(file.exists()){
            	List<String> currentLocalPaths = this.listLocalPaths(file);
                String transformedPath = currentPath.replace(File.separatorChar, '/');
                
                // add a empty resource for every sourcePath
                TNodeType element = new TNodeType();
                element.setTargetNamespace(this.getNamespace() + "resources/data-resources/file");
                element.setName(transformedPath + "/" + "Empty Resource");
                
                // define name propertie for empty resource to allow choice of the name of a new resource
                PropertiesDefinition prop = new PropertiesDefinition();
                prop.setElement(new QName(this.getNamespace() + "resources/data-resources/xsd/file", "EmptyResourceProperties"));
                element.setPropertiesDefinition(prop);
                
                // add QName and element to the lists
                entityList.add(element);
                this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/file", transformedPath + "/" + "Empty Resource"));

                for (String currentLocalPart : currentLocalPaths) {
                    // add QName with overall namespace prefix, file and the namespace part of this source as NamespaceURI
                    this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/file", transformedPath + "/" + currentLocalPart));
                    // create TOSCA NodeType for every data resource
                    element = new TNodeType(); // needs no properties definition because it represents an existing resource
                    element.setTargetNamespace(this.getNamespace() + "resources/data-resources/file");
                    element.setName(transformedPath + "/" + currentLocalPart);
                    element.setPropertiesDefinition(prop);
                    entityList.add(element);
                }	
            } 
        }

        return entityList;
    }
    
    public Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention) {
        if(!this.nameList.contains(resourceQname) || !this.deployableAlternativesQNames.contains(deployableType)){
            return null;
        }
        
        Deployable depl = null;
        
        // get the information needed to load the information of the resource represented by the QName
        String loadInformation = createDeployableStringForRequestedResource(resourceQname);
        
        // create deployable depending on the QNames
        if(resourceQname.getLocalPart().endsWith("Empty Resource")){
           try {
               depl = new DeployableObject(IOUtils.toInputStream(loadInformation, "UTF-8"), deployableType);
           } catch (IOException e) {
               e.printStackTrace();
           } 
        }else{
            try {
                int position = deployableAlternativesQNames.indexOf(deployableType);
                InputStream stream = IOUtils.toInputStream(loadInformation + "|" + deployableAlternatives.get(position), "UTF-8");
                depl = new DeployableObject(stream, deployableType);
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }
        return depl;
    }

    public List<QName> listDeployablesOfResource(QName resourceType) {
        // return the defined list of QNames from deployable alternatives
        return deployableAlternativesQNames;
    }

    public List<ImportObject> getImports() {
        // return List of Imports from last listEntities() call
        return this.importList;
    }

    public List<QName> getSupportedQNames() {
        // getSupportedQNames() requests depend on last listEntities() call
        // --> QNames list needs to be updated when a new listEntities() call is received
        return this.nameList;
    }
    
    /**
     * Create a String with all needed information to access the resource represented by the QName.
     * 
     * @param type The QName of the resource.
     * @return A String with the needed information.
     */
    private String createDeployableStringForRequestedResource(QName type){
        if (this.nameList.contains(type)) {
            
            for (String path : this.dataSources) {
                String transformedPath = path.replace(File.separatorChar, '/');
                if (type.getLocalPart().startsWith(transformedPath)) {
                    // create global path with correct formatting for easy access of the data
                    if (type.getLocalPart().endsWith("Empty Resource")) {
                        transformedPath = (transformedPath + File.separatorChar).replace('/', File.separatorChar);
                    } else {
                        transformedPath = (type.getLocalPart() + File.separatorChar).replace('/', File.separatorChar);
                    }
                    
                    // create resource engager with needed informations
                    String resourceEngager = type.getNamespaceURI() + ";";
                    resourceEngager += type.getLocalPart() + ";";
                    resourceEngager += transformedPath + ";";

                    return resourceEngager;
                }
            }
        }
        return null; //QName not supported
    }
    
    /**
     * List the local paths of all files contained in the source file.
     *
     * @param resource The source file for the listing.
     * @return The list containing all local paths.
     */
    private List<String> listLocalPaths(File resource) {
        // create the returned list
        List<String> paths = new ArrayList<String>();
        
        // create stack for searching all files in the resource and add resource location as starting point
        Stack<File> stack = new Stack<File>();
        stack.push(resource);
        
        while (stack.size() > 0) {
            for (File file : stack.pop().listFiles()) {
                if (file.isDirectory()) {
                    // read local path
                    String path = file.getAbsolutePath();
                    path = path.substring(resource.getAbsolutePath().length() + 1);
                    path = path.replace(File.separatorChar, '/');

                    // continue search and add path
                    stack.push(file);
                    paths.add(path);
                }
            }
        }
        return paths;
    }
}
