package uni_stuttgart.integration.domain_manager.implementations.data_resources;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TImport;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType.PropertiesDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.DeployableObject;
import uni_stuttgart.integration.domain_manager.IDataResourceDomainManager;
import uni_stuttgart.integration.domain_manager.ImportObject;
import uni_stuttgart.integration.domain_manager.XsdCreator;
import uni_stuttgart.integration.meta_data_objects.GitRepositoryMetaData;
import uni_stuttgart.integration.meta_data_objects.GitUserMetaData;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import de.uni_stuttgart.iaas.ipsm.v0.TIntention;


/**
 * Class for usage of git data sources. Uses the JGit library and the https
 * protocol for processing single git repositories. Uses the GitHub API and EGit
 * for retrieving all repositories of a user.
 *
 * @author wederbn
 */
@Service
public class GitDataResourceDomainManager implements IDataResourceDomainManager{

    // list to store current Imports
    private ArrayList<ImportObject> importList = new ArrayList<ImportObject>();
    // list to store current QNames
    private ArrayList<QName> nameList = new ArrayList<QName>();
    // list to store the meta-data for all available git repositories
    private ArrayList<GitRepositoryMetaData> dataSources = new ArrayList<GitRepositoryMetaData>();
    // list to store the meta-data for all available github usernames
    private ArrayList<GitUserMetaData> userDataSources = new ArrayList<GitUserMetaData>();
    // local path for cloning repositories
    private static final String localPath = "./Repos/";
    // context to load the ConfigurationProvider
    private AnnotationConfigApplicationContext context = null;
    // the namespace of the definitions documents that can be created by the aggregator this DM works for
    private String namespace;
    // list of all alternatives to store the data from this DM
    private ArrayList<String> deployableAlternatives = new ArrayList<String>();
    // list of all QNames of alternatives to store the data from this DM
    private ArrayList<QName> deployableAlternativesQNames = new ArrayList<QName>();


    private String getNamespace() {
        return this.namespace;
    }
    
    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }
    
    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }
    
    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }
    
    /**
     * Create a domain manager for git and load the meta data from all available
     * git data sources
     */
    public GitDataResourceDomainManager() {
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);
        
        // read meta data from properties file
        String[] urls = provider.getPropertyStringArray("GitDataResourceDomainManager.urlList");
        String[] repositoryUsernames = provider.getPropertyStringArray("GitDataResourceDomainManager.repositoryUsernameList");
        String[] repositoryPasswords = provider.getPropertyStringArray("GitDataResourceDomainManager.repositoryPasswordList");
        String[] githubUsernames = provider.getPropertyStringArray("GitDataResourceDomainManager.githubUsernameList");
        String[] githubPasswords = provider.getPropertyStringArray("GitDataResourceDomainManager.githubPasswordList");
        this.setNamespace(provider.getPropertyString("definitions.namespace"));
        
        // calculate number of correct inserted GitRepositoryMetaDatas
        int repositoryNumber = Math.min(urls.length, repositoryUsernames.length);
        repositoryNumber = Math.min(repositoryNumber, repositoryPasswords.length);
        
        // calculate number of correct inserted GitUserMetaDatas
        int githubNumber = Math.min(githubUsernames.length, githubPasswords.length);
        
        // Create MetaDataObjects from the data
        this.dataSources = new ArrayList<GitRepositoryMetaData>();
        for (int i = 0; i < repositoryNumber; i++) {
            this.dataSources.add(new GitRepositoryMetaData(urls[i], repositoryUsernames[i], repositoryPasswords[i]));
        }
        
        // Create MetaDataObjects from the data
        this.userDataSources = new ArrayList<GitUserMetaData>();
        for (int i = 0; i < githubNumber; i++) {
            this.userDataSources.add(new GitUserMetaData(githubUsernames[i], githubPasswords[i]));
        }
        
        // add deployable alternatives
        // alternative to store data in a git repo
        String resourceEngager = this.getNamespace() + "resources/data-resources/git/repository" + ";";
        resourceEngager += "Integrating-Test/Empty Resource" + ";"; 
        resourceEngager += "Integrating-Test" + ";";
        resourceEngager += "testtest1" + ";";
        deployableAlternatives.add(resourceEngager);
        deployableAlternativesQNames.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", "Integrating-Test/Empty Resource"));
        // alternative to store data in dropbox
        resourceEngager = this.getNamespace() + "resources/data-resources/dropbox" + ";";
        resourceEngager += "integrating.dataresources@web.de/Empty Resource" + ";";
        resourceEngager += "qXPJQq4y8JAAAAAAAAAACwNE3f7Z8Q9lT686lVFmQ9cuQT61nHrbWdpaCE7HuUie" + ";";
        resourceEngager += "integrating.dataresources@web.de" + ";";
        resourceEngager += "testtesttest" + ";";
        deployableAlternatives.add(resourceEngager);
        deployableAlternativesQNames.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", "integrating.dataresources@web.de/Empty Resource"));
    
    }
    
    public List<TExtensibleElements> listEntities() {
        // clear lists for new request
        this.importList.clear();
        this.nameList.clear();
        ArrayList<TExtensibleElements> entityList = new ArrayList<TExtensibleElements>();
        File file;
        
        // create TImport for usage in the definitions document
        TImport toscaImport = new TImport();
        toscaImport.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport.setNamespace(this.getNamespace() + "resources/data-resources/xsd/git");
        entityList.add(toscaImport);
        try {
            // create Import to describe the properties of the NodeTypes of this DM
            List<String> attributes = new LinkedList<String>();
            attributes.add("name");
            InputStream stream = XsdCreator.createXSD(this.getNamespace() + "resources/data-resources/xsd/git", "EmptyResourceProperties", attributes);
            ImportObject dmImport = new ImportObject(new URI(this.getNamespace() + "resources/data-resources/xsd/git"), new URI("http://www.w3.org/2001/XMLSchema"), stream);
            importList.add(dmImport);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // list of current repositories (all directly added repositories and all current repositories of the added users)
        List<GitRepositoryMetaData> currentRepositories = new ArrayList<GitRepositoryMetaData>();
        currentRepositories.addAll(this.dataSources);
        RepositoryService service = new RepositoryService();
        for (GitUserMetaData currentUser : this.userDataSources) {
            
            // add a empty repository resource for every available user
            TNodeType element = new TNodeType();
            element.setTargetNamespace(this.getNamespace() + "resources/data-resources/git/repository");
            element.setName(currentUser.getUsername() + "/" + "Empty Resource");
            
            // define name propertie for empty resource to allow choice of the name of a new resource
            PropertiesDefinition prop = new PropertiesDefinition();
            prop.setElement(new QName(this.getNamespace() + "resources/data-resources/xsd/git", "EmptyResourceProperties"));
            element.setPropertiesDefinition(prop);
            
            entityList.add(element);
            this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", currentUser.getUsername() + "/" + "Empty Resource"));
            
            try {
                // iterate over all repositories of the current user
                for (org.eclipse.egit.github.core.Repository repo : service.getRepositories(currentUser.getUsername())) {
                    GitRepositoryMetaData meta = new GitRepositoryMetaData(repo.getCloneUrl(), currentUser.getUsername(), currentUser.getPassword());
                    // check if repository was already added (static or through other user)
                    if(!currentRepositories.contains(meta)){
                        currentRepositories.add(meta);
                        // add a repository resource for every repository
                        element = new TNodeType();
                        element.setTargetNamespace(this.getNamespace() + "resources/data-resources/git/repository");
                        element.setName(currentUser.getUsername() + "/" + repo.getName());
                        element.setPropertiesDefinition(prop);
                        entityList.add(element);
                        this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", currentUser.getUsername() + "/" + repo.getName()));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        // process folder data resources of all repositories
        for (GitRepositoryMetaData currentSource : currentRepositories) {
            try {
                // determine local repository location and clone
                String temp = currentSource.getUrl().substring(8); //cut off https:// (change if git: is used)
                file = new File(GitDataResourceDomainManager.localPath + "/" + temp);
                this.cloneRepository(currentSource, file);
                
                // add a empty folder resource for every repository
                TNodeType element = new TNodeType();
                element.setTargetNamespace(this.getNamespace() + "resources/data-resources/git/folder");
                element.setName(temp + "/" + "Empty Resource");
                
                // define name propertie for empty resource to allow choice of the name of a new resource
                PropertiesDefinition prop = new PropertiesDefinition();
                prop.setElement(new QName(this.getNamespace() + "resources/data-resources/xsd/git", "EmptyResourceProperties"));
                element.setPropertiesDefinition(prop);
                
                entityList.add(element);
                this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/git/folder", temp + "/" + "Empty Resource"));
                
                List<String> localParts = this.listLocalPaths(file);
                for (String currentLocalPart : localParts) {
                    // add QName with overall namespace prefix, git and the namespace part of this source as NamespaceURI
                    // and path of the respective resource in the repository as local part
                    this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/git/folder", temp + "/" + currentLocalPart));
                    // create TOSCA NodeType for every data resource
                    element = new TNodeType();
                    element.setTargetNamespace(this.getNamespace() + "resources/data-resources/git/folder");
                    element.setName(temp + "/" + currentLocalPart);
                    element.setPropertiesDefinition(prop);
                    entityList.add(element);
                }
            } catch (Exception e) {
                e.printStackTrace();
                // data source not accessible (invalid url, username, pw; communictation failure;...);
                // --> skip data source
            }
        }
        return entityList;
    }
    
    public Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention) {
        if(!this.nameList.contains(resourceQname) || !this.deployableAlternativesQNames.contains(deployableType)){
            return null;
        }

        Deployable depl = null;
        
        // get the information needed to load the information of the resource represented by the QName
        String loadInformation = createDeployableStringForRequestedResource(resourceQname);

        // create deployable depending on the QNames
        if(resourceQname.getLocalPart().endsWith("Empty Resource")){
           try {
               depl = new DeployableObject(IOUtils.toInputStream(loadInformation, "UTF-8"), deployableType);
           } catch (IOException e) {
               e.printStackTrace();
           } 
        }else{
            try {
                int position = deployableAlternativesQNames.indexOf(deployableType);
                InputStream stream = IOUtils.toInputStream(loadInformation + "|" + deployableAlternatives.get(position), "UTF-8");
                depl = new DeployableObject(stream, deployableType);
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }
        return depl;
    }    

    public List<ImportObject> getImports() {
        // return List of Imports from last listEntities() call
        return this.importList;
    }
    
    public List<QName> listDeployablesOfResource(QName resourceType) {
        // return the defined list of QNames from deployable alternatives
        return deployableAlternativesQNames;
    }
    
    public List<QName> getSupportedQNames() {
        // getSupportedQNames() requests depend on last listEntities() call
        // --> QNames list needs to be updated when a new listEntities() call is received
        return this.nameList;
    }
    
    /**
     * Clone remote repository or checkout if already cloned
     *
     * @param source Meta-data about the repository that shall be cloned
     * @param file Location to which the repository shall be cloned
     * @throws GitAPIException
     * @throws TransportException
     * @throws InvalidRemoteException
     * @throws IOException
     */
    private void cloneRepository(GitRepositoryMetaData source, File file) throws InvalidRemoteException, TransportException, GitAPIException, IOException {
        // repository already cloned --> checkout and pull
        if (file.exists()) {
            FileRepositoryBuilder builder = new FileRepositoryBuilder();
            // search local repository
            Repository repository = builder.setGitDir(new File(file.getAbsolutePath() + File.separator + ".git")).readEnvironment().findGitDir().build();
            Git git = new Git(repository);
            // perform checkout and pull
            git.checkout().setName(Constants.MASTER).setCreateBranch(false).call();
            git.pull().setCredentialsProvider(new UsernamePasswordCredentialsProvider(source.getUsername(), source.getPassword())).call();
            git.close();
        } else {
            // clone repository
            Git.cloneRepository().setURI(source.getUrl()).setDirectory(file).setCredentialsProvider(new UsernamePasswordCredentialsProvider(source.getUsername(), source.getPassword())).call();
        }
    }  
    
    /**
     * List the local paths of all files contained in the source file.
     *
     * @param resource The source file for the listing.
     * @return The list containing all local paths.
     */
    private List<String> listLocalPaths(File resource) {
        // create the returned list
        List<String> paths = new ArrayList<String>();

        // create stack for searching all files in the resource and add resource location as starting point
        Stack<File> stack = new Stack<File>();
        stack.push(resource);

        while (stack.size() > 0) {
            for (File file : stack.pop().listFiles()) {
                if (file.isDirectory()) {
                    // read local path
                    String path = file.getAbsolutePath();
                    path = path.substring(resource.getAbsolutePath().length() + 1);
                    path = path.replace(File.separatorChar, '/');
                    // ignore git data
                    if(!path.startsWith(".git")){
                        // continue search and add path
                        stack.push(file);
                        paths.add(path);
                    }
                }
            }
        }
        return paths;
    }
    
    /**
     * Create a String with all needed information to access the resource represented by the QName.
     * 
     * @param type The QName of the resource.
     * @return A String with the needed information.
     */
    private String createDeployableStringForRequestedResource(QName type) {
        if (this.nameList.contains(type)) {
            String prefix = this.getNamespace() + "resources/data-resources/git/repository";
            if (type.getNamespaceURI().equals(prefix)) {
                // create Deployables for repositories
                String username = type.getLocalPart().split("/")[0];
                for (GitUserMetaData currentUser : this.userDataSources) {
                    // search associated username
                    if (currentUser.getUsername().equals(username)) {
                        // create resource engager with needed informations
                        String resourceEngager = type.getNamespaceURI() + ";";
                        resourceEngager += type.getLocalPart() + ";";
                        resourceEngager += currentUser.getUsername() + ";";
                        resourceEngager += currentUser.getPassword() + ";";

                        return resourceEngager;
                    }
                }
            } else {
                // create Deployables for folder resources
                
                // list of current repositories (all directly added repositories and all current repositories of the added users)
                List<GitRepositoryMetaData> currentRepositories = new ArrayList<GitRepositoryMetaData>();
                currentRepositories.addAll(this.dataSources);
                
                for (GitUserMetaData currentUser : this.userDataSources) {
                    // use authenticated requests to get a higher API request limit
                    GitHubClient client = new GitHubClient();
                    client.setCredentials(currentUser.getUsername(), currentUser.getPassword());
                    RepositoryService service = new RepositoryService(client);
                    try {
                        // iterate over all repositories of the current user
                        for (org.eclipse.egit.github.core.Repository repo : service.getRepositories(currentUser.getUsername())) {
                            GitRepositoryMetaData meta = new GitRepositoryMetaData(repo.getCloneUrl(), currentUser.getUsername(), currentUser.getPassword());
                            // check if repository was already added (static or through other user)
                            if (!currentRepositories.contains(meta)) {
                                currentRepositories.add(meta);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                
                // if QName is supported check which source supports it
                for (GitRepositoryMetaData currentSource : currentRepositories) {
                    // desired data source has to have the same local part
                    String currentNamespaceURI = currentSource.getUrl().substring(8);
                    
                    if (type.getLocalPart().startsWith(currentNamespaceURI)) {
                        // add information that are needed to access the desired data
                        String resourceEngager = type.getNamespaceURI() + ";";
                        resourceEngager += type.getLocalPart() + ";";
                        resourceEngager += currentSource.getUrl() + ";";
                        resourceEngager += currentSource.getUsername() + ";";
                        resourceEngager += currentSource.getPassword();
                        
                        return resourceEngager;
                    }
                }
            }
        }
        // return null if QName is not supported or an error occurred
        return null;
    }
}
