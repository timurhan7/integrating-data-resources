package uni_stuttgart.integration.domain_manager.implementations.data_resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TImport;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType.PropertiesDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v1.DbxClientV1;
import com.dropbox.core.v1.DbxEntry;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.DeployableObject;
import uni_stuttgart.integration.domain_manager.IDataResourceDomainManager;
import uni_stuttgart.integration.domain_manager.ImportObject;
import uni_stuttgart.integration.domain_manager.XsdCreator;
import uni_stuttgart.integration.meta_data_objects.DropboxMetaData;
import uni_stuttgart.ipsm.protocols.integration.Deployable;

/**
 * Class for usage of dropbox folders as data sources.
 * 
 * @author wederbn
 */
@Service
public class DropboxDataResourceDomainManager implements IDataResourceDomainManager{
    
    // list to store current Imports
    private ArrayList<ImportObject> importList = new ArrayList<ImportObject>();
    // list to store current QNames
    private ArrayList<QName> nameList = new ArrayList<QName>();
    // context to load the ConfigurationProvider
    private AnnotationConfigApplicationContext context = null;
    // the namespace of the definitions documents that can be created by the aggregator this DM works for
    private String namespace;
    // list of all alternatives to store the data from this DM
    private ArrayList<String> deployableAlternatives = new ArrayList<String>();
    // list of all QNames of alternatives to store the data from this DM
    private ArrayList<QName> deployableAlternativesQNames = new ArrayList<QName>();
    // list to store the meta-data for all available dropbox accounts
    private ArrayList<DropboxMetaData> dataSources = new ArrayList<DropboxMetaData>();
   
    
    private String getNamespace() {
        return this.namespace;
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }

    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }
    
    public DropboxDataResourceDomainManager() {
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);

        // read meta data from properties file
        String[] accessTokens = provider.getPropertyStringArray("Dropbox.accessToken");
        String[] usernames = provider.getPropertyStringArray("Dropbox.username");
        String[] passwords = provider.getPropertyStringArray("Dropbox.password");
        this.setNamespace(provider.getPropertyString("definitions.namespace"));

        // get number of correct inserted accounts
        int accountNumber = Math.min(accessTokens.length, usernames.length);
        accountNumber = Math.min(accountNumber, passwords.length);
        
        // store data sources in a list
        this.dataSources = new ArrayList<DropboxMetaData>();
        for (int i = 0; i < accountNumber; i++) {
            this.dataSources.add(new DropboxMetaData(accessTokens[i], usernames[i], passwords[i]));
        }
        
        // add deployable alternatives
        // alternative to store data in a git repo
        String resourceEngager = this.getNamespace() + "resources/data-resources/git/repository" + ";";
        resourceEngager += "Integrating-Test/Empty Resource" + ";";
        resourceEngager += "Integrating-Test" + ";";
        resourceEngager += "testtest1" + ";";
        deployableAlternatives.add(resourceEngager);
        deployableAlternativesQNames.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", "Integrating-Test/Empty Resource"));
        // alternative to store data in dropbox
        resourceEngager = this.getNamespace() + "resources/data-resources/dropbox" + ";";
        resourceEngager += "integrating.dataresources@web.de/Empty Resource" + ";";
        resourceEngager += "qXPJQq4y8JAAAAAAAAAACwNE3f7Z8Q9lT686lVFmQ9cuQT61nHrbWdpaCE7HuUie" + ";";
        resourceEngager += "integrating.dataresources@web.de" + ";";
        resourceEngager += "testtesttest" + ";";
        deployableAlternatives.add(resourceEngager);
        deployableAlternativesQNames.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", "integrating.dataresources@web.de/Empty Resource"));
    }

    public List<TExtensibleElements> listEntities() {
        // clear lists for new request
        this.importList.clear();
        this.nameList.clear();
        ArrayList<TExtensibleElements> entityList = new ArrayList<TExtensibleElements>();
        
        // create TImport for usage in the definitions document
        TImport toscaImport = new TImport();
        toscaImport.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport.setNamespace(this.getNamespace() + "resources/data-resources/xsd/dropbox");
        entityList.add(toscaImport);
        try {
            // create Import to describe the properties of the NodeTypes of this DM
            List<String> attributes = new LinkedList<String>();
            attributes.add("name");
            InputStream stream = XsdCreator.createXSD(this.getNamespace() + "resources/data-resources/xsd/dropbox", "EmptyResourceProperties", attributes);
            ImportObject dmImport = new ImportObject(new URI(this.getNamespace() + "resources/data-resources/xsd/dropbox"), new URI("http://www.w3.org/2001/XMLSchema"), stream);
            importList.add(dmImport);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        for(DropboxMetaData meta : this.dataSources){
            // add a empty resource for every sourcePath
            TNodeType element = new TNodeType();
            element.setTargetNamespace(this.getNamespace() + "resources/data-resources/dropbox");
            element.setName(meta.getUsername() + "/" + "Empty Resource");
            
            // define name propertie for empty resource to allow choice of the name of a new resource
            PropertiesDefinition prop = new PropertiesDefinition();
            prop.setElement(new QName(this.getNamespace() + "resources/data-resources/xsd/dropbox", "EmptyResourceProperties"));
            element.setPropertiesDefinition(prop);
            
            // add QName and element to the lists
            entityList.add(element);
            this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", meta.getUsername() + "/" + "Empty Resource"));
            
            // add all folder resources in this data source
            try{
                DbxRequestConfig config = new DbxRequestConfig("", Locale.getDefault().toString());
                DbxClientV1 client = new DbxClientV1(config, meta.getAccessToken());
                entityList.addAll(getSubfoldersAsExtensibleElements("/", client, meta.getUsername())); 
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return entityList;
    }

    public List<ImportObject> getImports() {
        // return List of Imports from last listEntities() call
        return this.importList;
    }

    public List<QName> getSupportedQNames() {
        // getSupportedQNames() requests depend on last listEntities() call
        // --> QNames list needs to be updated when a new listEntities() call is received
        return this.nameList;
    }

    public Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention) {
        if(!this.nameList.contains(resourceQname) || !this.deployableAlternativesQNames.contains(deployableType)){
            return null;
        }

        Deployable depl = null;
        
        // get the information needed to load the information of the resource represented by the QName
        String loadInformation = createDeployableStringForRequestedResource(resourceQname);
        
        // create deployable depending on the QNames
        if(resourceQname.getLocalPart().endsWith("Empty Resource")){
           try {
               depl = new DeployableObject(IOUtils.toInputStream(loadInformation, "UTF-8"), deployableType);
           } catch (IOException e) {
               e.printStackTrace();
           } 
        }else{
            try {
                int position = deployableAlternativesQNames.indexOf(deployableType);
                InputStream stream = IOUtils.toInputStream(loadInformation + "|" + deployableAlternatives.get(position), "UTF-8");
                depl = new DeployableObject(stream, deployableType);
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }
        return depl;
    }

    private String createDeployableStringForRequestedResource(QName resourceQname) {
        if (this.nameList.contains(resourceQname)) {
            String username = resourceQname.getLocalPart().split("/")[0];
            for (DropboxMetaData source : this.dataSources) {
                if(source.getUsername().equals(username)){
                    // create resource engager with needed informations
                    String resourceEngager = resourceQname.getNamespaceURI() + ";";
                    resourceEngager += resourceQname.getLocalPart() + ";";
                    resourceEngager += source.getAccessToken() + ";";
                    resourceEngager += source.getUsername() + ";";
                    resourceEngager += source.getPassword() + ";";
                    
                    return resourceEngager; 
                }
            }
        }

        return null;
    }

    public List<QName> listDeployablesOfResource(QName resourceType) {
        // return the defined list of QNames from deployable alternatives
        return deployableAlternativesQNames;
    }
    
    /**
     * Return all Folders contained in a source folder as TNodeType.
     * 
     * @param path The path to the source folder.
     * @param client The dropbox client to connect to the dropbox account containing the source folder.
     * @param username The username for the dropbox account.
     * @return A list containing all sub folders of the source folder.
     */
    private List<TExtensibleElements> getSubfoldersAsExtensibleElements(String path, DbxClientV1 client, String username) throws Exception{
        List<TExtensibleElements> files = new ArrayList<TExtensibleElements>();
        
        // get all children of the path
        DbxEntry.WithChildren listing = client.getMetadataWithChildren(path);
        if(!path.endsWith("/")){
            path = path + "/"; 
        }
        for(DbxEntry child : listing.children){
            // search recursive for further files
            if(child.isFolder()){
                TNodeType element = new TNodeType();
                element.setTargetNamespace(this.getNamespace() + "resources/data-resources/dropbox");
                element.setName(username + path + child.name);
                files.add(element);
                this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", username + path + child.name));
                // search recursive
                files.addAll(getSubfoldersAsExtensibleElements(path + child.name, client, username));
            }
        }
        return files;
    }
}
