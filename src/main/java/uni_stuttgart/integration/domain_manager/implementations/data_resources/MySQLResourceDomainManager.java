package uni_stuttgart.integration.domain_manager.implementations.data_resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TImport;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType.PropertiesDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.PullImageResultCallback;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.DeployableObject;
import uni_stuttgart.integration.domain_manager.IDataResourceDomainManager;
import uni_stuttgart.integration.domain_manager.ImportObject;
import uni_stuttgart.integration.domain_manager.XsdCreator;
import uni_stuttgart.integration.meta_data_objects.DockerMetaData;
import uni_stuttgart.ipsm.protocols.integration.Deployable;

/**
 * Class for usage of MySQL databases and tables as data sources. Uses docker clients to 
 * retrieve the different active MySQL instances in docker containers of the 
 * related docker daemons. As docker image the mysql image is used.
 *
 * @author wederbn
 */
@Service
public class MySQLResourceDomainManager implements IDataResourceDomainManager{

    // list to store current Imports
    private ArrayList<ImportObject> importList = new ArrayList<ImportObject>();
    // list to store current QNames
    private ArrayList<QName> nameList = new ArrayList<QName>();
    // context to load the ConfigurationProvider
    private AnnotationConfigApplicationContext context = null;
    // the namespace of the definitions documents that can be created by the aggregator this DM works for
    private String namespace;
    // list to store the meta-data for all available docker clients
    private ArrayList<DockerMetaData> dataSources = new ArrayList<DockerMetaData>();
    
    private String getNamespace() {
        return this.namespace;
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }

    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }
    
    /**
     * Create a domain manager for MySQl and load the meta data 
     * from all available docker clients, which possibly contain
     * MySQL instances.
     */
    public MySQLResourceDomainManager(){
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);
        
        // read meta data from properties file
        this.setNamespace(provider.getPropertyString("definitions.namespace"));
        String[] urls = provider.getPropertyStringArray("Docker.uri");
        String[] certPaths = provider.getPropertyStringArray("Docker.certs");
        
        // calculate number of correct inserted GitRepositoryMetaDatas
        int dockerClientNumber = Math.min(certPaths.length, urls.length);
        
        // save meta-data of the docker clients
        this.dataSources = new ArrayList<DockerMetaData>();
        for (int i = 0; i < dockerClientNumber; i++) {
            this.dataSources.add(new DockerMetaData(urls[i], certPaths[i]));
        }
    }

    public List<TExtensibleElements> listEntities() {
        // clear lists for new request
        this.importList.clear();
        this.nameList.clear();
        ArrayList<TExtensibleElements> entityList = new ArrayList<TExtensibleElements>();
        
        // create TImport for databases for usage in the definitions document
        TImport toscaImport = new TImport();
        toscaImport.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport.setNamespace(this.getNamespace() + "resources/data-resources/xsd/mysql");
        entityList.add(toscaImport);
        
        // create TImport for tables for usage in the definitions document
        toscaImport = new TImport();
        toscaImport.setImportType("http://www.w3.org/2001/XMLSchema");
        toscaImport.setNamespace(this.getNamespace() + "resources/data-resources/xsd/tables/mysql");
        entityList.add(toscaImport);
        try {
            // create Import to describe the properties of the database NodeTypes of this DM
            List<String> attributes = new LinkedList<String>();
            attributes.add("dbName");
            attributes.add("userName");
            attributes.add("password");
            InputStream stream = XsdCreator.createXSD(this.getNamespace() + "resources/data-resources/xsd/mysql", "MySqlProperties", attributes);
            ImportObject dmImport = new ImportObject(new URI(this.getNamespace() + "resources/data-resources/xsd/mysql"), new URI("http://www.w3.org/2001/XMLSchema"), stream);
            importList.add(dmImport);
            
            // create Import to describe the properties of the table NodeTypes of this DM
            attributes = new LinkedList<String>();
            attributes.add("dbName");
            attributes.add("userName");
            attributes.add("password");
            attributes.add("tableName");
            attributes.add("listOfColumnNames");
            attributes.add("listOfColumnTypes");
            attributes.add("listOfPrimaryKeys");
            stream = XsdCreator.createXSD(this.getNamespace() + "resources/data-resources/xsd/tables/mysql", "TableProperties", attributes);
            dmImport = new ImportObject(new URI(this.getNamespace() + "resources/data-resources/xsd/tables/mysql"), new URI("http://www.w3.org/2001/XMLSchema"), stream);
            importList.add(dmImport);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // iterate over every docker meta-data set
        for (DockerMetaData currentSource : this.dataSources) {
            // create docker client
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(currentSource.getURI()).withDockerCertPath(currentSource.getCertPath());
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            String uri = currentSource.getURI().substring(6); //cut of tcp://
            
            // pull the needed Image if not already done
            dockerClient.pullImageCmd("mysql:latest").exec(new PullImageResultCallback()).awaitSuccess();
            
            // define properties
            PropertiesDefinition prop = new PropertiesDefinition();
            prop.setElement(new QName(this.getNamespace() + "resources/data-resources/xsd/mysql", "MySqlProperties"));
            
            //iterate over all available images and add a empty resource for each
            List<Image> images = dockerClient.listImagesCmd().exec();
            for(Image image : images){
                for(String tag : image.getRepoTags()){
                    if(tag.startsWith("mysql")){
                        String tagName;
                        if(tag.equals("mysql:latest")){
                            // new unchanged MySql
                            tagName = "newMySql";
                        }else{
                            // saved changed MediaWiki
                            tagName = tag.split(":")[1];
                        }
                        // add a empty resource for every docker client
                        TNodeType element = new TNodeType();
                        element.setTargetNamespace(this.getNamespace() + "resources/data-resources/mysql");
                        element.setName(uri + "/" + tagName + "/" + "Empty Resource");
                        
                        // add properties element
                        element.setPropertiesDefinition(prop);
                        
                        entityList.add(element);
                        this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/mysql", uri + "/" + tagName + "/" + "Empty Resource"));      
                    }
                }
            }   
            
            // iterate over all containers in the docker client
            List<Container> containers = dockerClient.listContainersCmd().exec();
            for(Container container : containers){
                if(container.getImage().startsWith("mysql")){
                    String databaseName = container.getNames()[0].substring(1); // read name and cut of the slash at the beginning
                    
                    // add a database resource for every container with the mysql image
                    TNodeType element = new TNodeType();
                    element.setTargetNamespace(this.getNamespace() + "resource-instances/data-resources/mysql");
                    element.setName(uri + "/" + databaseName); 

                    // add properties element
                    element.setPropertiesDefinition(prop);
                    
                    entityList.add(element);
                    this.nameList.add(new QName(this.getNamespace() + "resource-instances/data-resources/mysql", uri + "/" + databaseName));
                    
                    
                    // add a empty table resource for every database to enable the creation of new tables
                    element = new TNodeType();
                    element.setTargetNamespace(this.getNamespace() + "resources/data-resources/tables/mysql");
                    element.setName(uri + "/" + databaseName + "/" + "Empty Resource");

                    // define properties
                    PropertiesDefinition propTable = new PropertiesDefinition();
                    propTable.setElement(new QName(this.getNamespace() + "resources/data-resources/xsd/tables/mysql", "TableProperties"));
                    element.setPropertiesDefinition(propTable);
                    
                    entityList.add(element);
                    this.nameList.add(new QName(this.getNamespace() + "resources/data-resources/tables/mysql", uri + "/" + databaseName + "/" + "Empty Resource"));
                }
            }
        }
        return entityList;
    }

    public Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention) {
        // resourceQname and deployableType need to be the same for mysql
        if(!this.nameList.contains(resourceQname) || !resourceQname.equals(deployableType)){
            return null;
        }
        
        // get the information needed to access the data of the resource represented by the QName
        Deployable depl = null;
        String loadInformation = createDeployableStringForRequestedResource(resourceQname);
        
        if(resourceQname.getLocalPart().endsWith("Empty Resource")){
            try {
                InputStream stream = IOUtils.toInputStream(" |" + loadInformation, "UTF-8");
                depl = new DeployableObject(stream, deployableType);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                InputStream stream = IOUtils.toInputStream(loadInformation + "| ", "UTF-8");
                depl = new DeployableObject(stream, deployableType);
            } catch (IOException e) {
                e.printStackTrace();
            } 
        }
        return depl;
    }

    public List<QName> listDeployablesOfResource(QName resourceType) {
        List<QName> nameList = new ArrayList<QName>();
        // for mysql instances just one store format exists
        nameList.add(resourceType);
        return nameList;
    }

    public String createDeployableStringForRequestedResource(QName type) {
        // check if QName is supported by this DM
        if (this.nameList.contains(type)) {
            String uri = "tcp://" + type.getLocalPart().split("/")[0]; // get docker uri
            
            // search QName related Docker Client
            for (DockerMetaData currentSource : this.dataSources) {
                if (uri.equals(currentSource.getURI())) {
                    // add information for accessing the resource
                    String resourceEngager = type.getNamespaceURI() + ";";
                    resourceEngager += type.getLocalPart() + ";";
                    resourceEngager += currentSource.getURI() + ";";
                    resourceEngager += currentSource.getCertPath() + ";";
                    
                    return resourceEngager;
                }
            }
        }
        // return null if QName is not supported or an error occurred
        return null;
    }

    public List<ImportObject> getImports() {
        // return List of Imports from last listEntities() call
        return this.importList;
    }

    public List<QName> getSupportedQNames() {
        // getSupportedQNames() requests depend on last listEntities() call
        return this.nameList;
    }
}
