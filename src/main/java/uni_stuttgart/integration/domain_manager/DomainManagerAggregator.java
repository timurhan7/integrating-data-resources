package uni_stuttgart.integration.domain_manager;


import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TImport;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations;
import uni_stuttgart.ipsm.protocols.integration.Import;
import de.uni_stuttgart.iaas.ipsm.v0.TIntention;

/**
 * Class for aggregation of the functionality of all created Domain Managers.
 * @author wederbn
 *
 */
public class DomainManagerAggregator implements DomainManagerOperations{
    // list of available Domain Managers for aggregation
    private List<IDataResourceDomainManager> dataSources;
    // context to load implementations of the IDataResourceDomainManager interface
    private AnnotationConfigApplicationContext context = null;
    // the name of the definitions documents that can be created
    private String name;
    // the namespace of the definitions documents that can be created
    private URI namespace;
    
    private void setNamespace(URI namespace) {
        this.namespace = namespace;
    }
    
    private String getName() {
        return this.name;
    }
    
    private void setName(String name) {
        this.name = name;
    }
    
    private List<IDataResourceDomainManager> getDataSources() {
        return this.dataSources;
    }
    
    private void setDataSources(List<IDataResourceDomainManager> dataSources) {
        this.dataSources = dataSources;
    }
    
    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }
    
    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context= annotationConfigApplicationContext;		
    }
    
    /**
     * Create DomainManagerAggregator and load dependencies (name, namespace, IDataResourceDomainManager)
     * @throws URISyntaxException 
     */
    public DomainManagerAggregator() throws URISyntaxException{
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);
        
        // read needed configurations from properties file through ConfigurationProvider
        this.setName(provider.getPropertyString("definitions.name"));
        this.setNamespace(new URI(provider.getPropertyString("definitions.namespace")));
        String[] packages = provider.getPropertyStringArray("IDataResourceDomainManager.locations");
        // scan desired packages for implementations and add them to the list
        
        ArrayList<IDataResourceDomainManager> list = new ArrayList<IDataResourceDomainManager>();
        for(String currentPackage : packages){
            try{
                this.setContext(new AnnotationConfigApplicationContext());
                this.getContext().scan(currentPackage);
                this.getContext().refresh();
                list.addAll(this.getContext().getBeansOfType(IDataResourceDomainManager.class).values());
            }catch(BeansException e){
                // package string was not valid
            }
        }
        this.setDataSources(list);
    }
    
    public Definitions listDomain() {
        // create new Definitions Document and insert id, name and namespace
        Definitions definitions = new Definitions();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        definitions.setId(this.getName() + "-" + dateFormat.format(date));
        definitions.setName(this.getName());
        definitions.setTargetNamespace(this.getTargetNamespace().toString());
        
        // insert TEntityTemplates from every IDataResourceDomainManager into Definitions Document
        for(IDataResourceDomainManager currentSource : this.getDataSources()){
            List<TExtensibleElements> currentElements = currentSource.listEntities();
            if(currentElements != null){
                for(TExtensibleElements currentType : currentElements){
                    // distinguish between Imports and other Tosca Types and add to list of definitions
                    if(currentType.getClass().equals(TImport.class)){
                        definitions.getImport().add((TImport) currentType);
                    }else{
                        definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().add(currentType);
                    }
                }
            }	
        }
        return definitions;
    }
    
    public String getTargetExecutionEnvironment(String resourceType) {
        return null; // method is deprecated
    }
    
    public List<Import> getImports() {
        ArrayList<Import> imports = new ArrayList<Import>();
        // iterate over all Domain Manager and add imports
        for(IDataResourceDomainManager currentSource : this.getDataSources()){
            if(currentSource.getImports() != null){
                for(ImportObject currentImport : currentSource.getImports()){
                    // add only imports that are not yet in the list
                    if(!imports.contains(currentImport)){
                        imports.add(currentImport);
                    }
                }
            }
        }
        return imports;
    }

    public URI getTargetNamespace() {
        return this.namespace;
    }

    public Deployable getDeployable(QName resourceQname, QName deployableType, TIntention intention) {
     // check if any source supports the desired type and forward the request to that source
        for(IDataResourceDomainManager currentSource : this.getDataSources()){
            if(currentSource.getSupportedQNames()!= null){
                if(currentSource.getSupportedQNames().contains(resourceQname)){  
                    return currentSource.getDeployable(resourceQname, deployableType, intention);
                }
            }
        }
        return null; //QName is not supported by the current data sources
    }

    public List<QName> listDeployablesOfResource(QName resourceType) {
        // check if any source supports the desired type and forward the request to that source
        for(IDataResourceDomainManager currentSource : this.getDataSources()){
            if(currentSource.getSupportedQNames()!= null){
                if(currentSource.getSupportedQNames().contains(resourceType)){  
                    return currentSource.listDeployablesOfResource(resourceType);
                }
            }
        }
        return null; //QName is not supported by the current data sources
    }
}
