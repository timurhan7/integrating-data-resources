package uni_stuttgart.integration.domain_manager;

import java.io.InputStream;
import java.net.URI;

import uni_stuttgart.ipsm.protocols.integration.Import;

/**
 * Class represents a import object of a Domain Manager and is used by
 * {@link IDataResourceDomainManager}.
 * 
 * @author wederbn
 *
 */
public class ImportObject implements Import{

    private URI namespace;
    private URI importType;
    private InputStream stream;


    public ImportObject(URI namespace, URI importType, InputStream stream) {
        this.namespace = namespace;
        this.importType = importType;
        this.stream = stream;
    }
    
    public URI getNamespace() {
        return this.namespace;
    }
    
    public URI getImportType() {
        return this.importType;
    }
    
    public InputStream getImport() {
        return this.stream;
    }
    
    @Override
    public boolean equals(Object obj) {
        
        if (obj == null) {
            return false;
        }
        
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        
        if (((ImportObject) obj).getNamespace().equals(this.namespace) && ((ImportObject) obj).getImportType().equals(this.importType)) {
            return true;
        }
        
        return false;
        
    }
    
}
