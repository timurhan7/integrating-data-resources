package uni_stuttgart.integration.domain_manager;

import java.io.InputStream;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;

import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Represents a resource engager for testing purposes
 *
 * @author wederbn
 */
public class RunnableContainerObject implements RunnableContainer {
    
    private TEntityTemplate object;
    private InputStream stream;
    private QName type;
    
    
    public RunnableContainerObject(TEntityTemplate object, InputStream stream, QName type) {
        this.object = object;
        this.stream = stream;
        this.type = type;
    }
    
    public TEntityTemplate getTargetModel() {
        return this.object;
    }
    
    public InputStream getDeployable() {
        return this.stream;
    }

    public QName getType() {
        return this.type;
    }

}
