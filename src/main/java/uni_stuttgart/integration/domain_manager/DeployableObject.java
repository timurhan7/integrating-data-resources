package uni_stuttgart.integration.domain_manager;

import java.io.InputStream;

import javax.xml.namespace.QName;

import uni_stuttgart.ipsm.protocols.integration.Deployable;

/**
 * Represents a deployable object.
 * @author wederbn
 *
 */
public class DeployableObject implements Deployable{
    
    private InputStream stream;
    private QName type;
    
    
    public DeployableObject(InputStream stream, QName type) {
        this.stream = stream;
        this.type = type;
    }

    @Deprecated
    public String getTargetRuntime() {
        return null;
    }

    public InputStream getDeployable() {
        return this.stream;
    }

    public QName getType() {
        return this.type;
    }

}
