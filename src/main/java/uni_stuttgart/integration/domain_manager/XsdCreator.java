package uni_stuttgart.integration.domain_manager;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class XsdCreator {
    
    /**
     * Create a XSD-File with the parameters.
     * 
     * @param targetNamespace The targetNamespace of the XSD-File.
     * @param element The name of the element in the XSD-File.
     * @param attributes A list of all attribut names. The type of the attributes is xs:string.
     * @return A XSD-File containing an element with the specified attributes.
     * @throws IOException
     */
    public static InputStream createXSD(String targetNamespace, String element, List<String> attributes) throws IOException{
        String xsdFile = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        xsdFile += "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" ";
        xsdFile += "targetNamespace=\"" + targetNamespace + "\">";
        xsdFile += "<xs:element name=\"" + element + "\">";
        xsdFile += "<xs:complexType>";
        for(String attribute : attributes){
           xsdFile += "<xs:attribute name=\"" + attribute + "\" type=\"xs:string\"/>"; 
        }  
        xsdFile += "</xs:complexType>";
        xsdFile += "</xs:element>";
        xsdFile += "</xs:schema>";
        return IOUtils.toInputStream(xsdFile, "UTF-8");  
    }

    // create more than one element in one schema file
//    static InputStream createXSD(String targetNamespace, List<String> elements, List<List<String>> attributes) throws IOException{
//        String xsdFile = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
//        xsdFile += "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" ";
//        xsdFile += "targetNamespace=\"" + targetNamespace + "\">";
//        for(int i = 0; i < elements.size(); i++){
//            xsdFile += "<xs:element name=\"" + elements.get(i) + "\">";
//            xsdFile += "<xs:complexType>";
//            for(String attribute : attributes.get(i)){
//                xsdFile += "<xs:attribute name=\"" + attribute + "\" type=\"xs:string\"/>"; 
//             }  
//            xsdFile += "</xs:complexType>";
//            xsdFile += "</xs:element>";
//        }
//        xsdFile += "</xs:schema>";
//        return IOUtils.toInputStream(xsdFile, "UTF-8");  
//    }
}
