package uni_stuttgart.integration.meta_data_objects;

/**
 * Class for meta-data of a docker client.
 *
 * @author wederbn
 *
 */
public class DockerMetaData {
    private String uri;
    private String certPath;

    
    public DockerMetaData(String uri, String certPath) {
        this.certPath = certPath;
        this.uri = uri;
    }
    
    public String getURI() {
        return this.uri;
    };
    
    public String getCertPath() {
        return this.certPath;
    };
}
