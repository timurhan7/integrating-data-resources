package uni_stuttgart.integration.meta_data_objects;

/**
 * Class for meta-data of a github user.
 *
 * @author wederbn
 *
 */
public class GitUserMetaData {
    
    private String username;
    private String password;

    
    public GitUserMetaData(String username, String password) {
        this.password = password;
        this.username = username;
    }
    
    public String getUsername() {
        return this.username;
    };
    
    public String getPassword() {
        return this.password;
    };
}
