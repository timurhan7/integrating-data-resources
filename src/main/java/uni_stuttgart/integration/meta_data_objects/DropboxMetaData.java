package uni_stuttgart.integration.meta_data_objects;

/**
 *  Class for meta-data of a dropbox accounts.
 * 
 * @author wederbn
 *
 */

public class DropboxMetaData {
    private String accessToken;
    private String username;
    private String password;

    
    public DropboxMetaData(String accessToken, String username, String password) {
        this.accessToken = accessToken;
        this.username = username;
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }


    public String getUsername() {
        return username;
    }


    public String getPassword() {
        return password;
    }
}
