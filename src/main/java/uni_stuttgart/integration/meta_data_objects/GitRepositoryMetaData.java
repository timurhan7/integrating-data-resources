package uni_stuttgart.integration.meta_data_objects;

/**
 * Class for meta-data of a git repository.
 *
 * @author wederbn
 *
 */
public class GitRepositoryMetaData {

    private String url;
    private String username;
    private String password;


    public GitRepositoryMetaData(String url, String username, String password) {
        this.password = password;
        this.url = url;
        this.username = username;
    }
    
    public String getUrl() {
        return this.url;
    };
    
    public String getUsername() {
        return this.username;
    };
    
    public String getPassword() {
        return this.password;
    };
    
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof GitRepositoryMetaData)) {
            return false;
        }
        GitRepositoryMetaData testObject = (GitRepositoryMetaData) other;
        if (testObject.getUrl().equals(this.getUrl()) && testObject.getUsername().equals(this.getUsername()) && testObject.getPassword().equals(this.getPassword())) {
            return true;
        } else {
            return false;
        }
    }
}
