package uni_stuttgart.integration.eei;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage;
import de.uni_stuttgart.iaas.ipsm.v0.TOtherParameters;
import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;
import uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation;

/**
 * Class for execution of the acquire operation on resources.
 * The operation accesses data and provides it in a desired data source for the user.
 * @author wederbn
 *
 */
public class AcquireInformationResources extends AcquireResourceOperation{
    
    // list of available Data Loader for accessing data
    private List<IDataLoader> dataLoader;
    // list of available Data Storages for providing the accessed data
    private List<IDataStorage> dataStorage;
    // context to load implementations of the IDataLoader and IDataStorage interfaces
    private AnnotationConfigApplicationContext context = null;
    // the overall namespace
    private String namespace;
    // list of all QNames of alternatives to store file data
    private ArrayList<QName> deployableAlternativesForFileResources = new ArrayList<QName>();
    
    private String getNamespace() {
        return this.namespace;
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }
    
    private List<IDataLoader> getDataLoader() {
        return this.dataLoader;
    }

    private void setDataLoader(List<IDataLoader> dataLoader) {
        this.dataLoader = dataLoader;
    }

    private List<IDataStorage> getDataStorage() {
        return this.dataStorage;
    }

    private void setDataStorage(List<IDataStorage> dataStorage) {
        this.dataStorage = dataStorage;
    }

    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }

    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }

    /**
     * Create AcquireResourceOperationInstance and load dependencies
     * (IDataLoader and IDataStorage)
     */
    public AcquireInformationResources() {
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);

        // read namespace
        this.setNamespace(provider.getPropertyString("definitions.namespace"));
        
        // read package names
        String[] loaderPackages = provider.getPropertyStringArray("IDataLoader.locations");
        String[] storagePackages = provider.getPropertyStringArray("IDataStorage.locations");

        // scan desired packages for implementations of IDataLoader and add them to the list
        ArrayList<IDataLoader> loaderList = new ArrayList<IDataLoader>();
        for (String currentPackage : loaderPackages) {
            try {
                this.setContext(new AnnotationConfigApplicationContext());
                this.getContext().scan(currentPackage);
                this.getContext().refresh();
                loaderList.addAll(this.getContext().getBeansOfType(IDataLoader.class).values());
            } catch (BeansException e) {
                // package string was not valid
            }
        }
        this.setDataLoader(loaderList);

        // add implementations of IDataStorage to the list
        ArrayList<IDataStorage> storageList = new ArrayList<IDataStorage>();
        for (String currentPackage : storagePackages) {
            try {
                this.setContext(new AnnotationConfigApplicationContext());
                this.getContext().scan(currentPackage);
                this.getContext().refresh();
                storageList.addAll(this.getContext().getBeansOfType(IDataStorage.class).values());
            } catch (BeansException e) {
                // package string was not valid
            }
        }
        this.setDataStorage(storageList);
        
        // add deployable alternatives for file data
        deployableAlternativesForFileResources.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", "integrating.dataresources@web.de/Empty Resource"));
        deployableAlternativesForFileResources.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", "Integrating-Test/Empty Resource"));
    }

    public void executeOperation(List<RunnableContainer> resourceOrRelationshipContainer, Object inputParameters, OperationCallback callback) {
        try {
            // complete on Error if no or more than two RunnableContainers are passed
            if(resourceOrRelationshipContainer == null || resourceOrRelationshipContainer.size() > 2){
                this.completeProcessExecutionwithError("No or invalid number of RunnableContainers passed", callback);
                return;
            }
            
            RunnableContainer completeRunnableContainer = resourceOrRelationshipContainer.get(0);
            RunnableContainer sourceResource = null;
            RunnableContainer targetResource = null;
            
            // find source and target resource if passed
            for(int i = 0; i < resourceOrRelationshipContainer.size(); i++){
                RunnableContainer temp = resourceOrRelationshipContainer.get(i);
                if(temp != null){
                    if(temp.getType().getLocalPart().equals("source-resource")){
                        sourceResource = temp;
                    }
                    if(temp.getType().getLocalPart().equals("target-resource")){
                        targetResource = temp;
                    }
                }
            }

            // parse RunnableContainer in sourceResource and targetResource
            if(targetResource == null || sourceResource == null){
                // read the data from the completeRunnableContainer
                String accessData = IOUtils.toString(completeRunnableContainer.getDeployable(), "UTF-8");
                String[] dataArray = accessData.split("\\|");

                if(dataArray.length == 1){
                    // empty resource only needs a targetResource
                    sourceResource = null;
                    InputStream targetResourceEngager = IOUtils.toInputStream(dataArray[0], "UTF-8");
                    targetResource = new RunnableContainerObject(completeRunnableContainer.getTargetModel(), targetResourceEngager, completeRunnableContainer.getType());
                }else{
                    // other resources need a source and target Resource
                    if(!dataArray[0].equals(" ")){
                        InputStream sourceResourceEngager = IOUtils.toInputStream(dataArray[0], "UTF-8");
                        sourceResource = new RunnableContainerObject(completeRunnableContainer.getTargetModel(), sourceResourceEngager, completeRunnableContainer.getType());
                    }else{
                        sourceResource = null;
                    }
                    if(!dataArray[1].equals(" ")){
                        InputStream targetResourceEngager = IOUtils.toInputStream(dataArray[1], "UTF-8");
                        targetResource = new RunnableContainerObject(completeRunnableContainer.getTargetModel(), targetResourceEngager, completeRunnableContainer.getType());
                    }else{
                        targetResource = null;
                    }
                }
            }
            
            List<FileObject> accessedData = null;
            // check if there is data to load for the newly created resource or a empty resource is desired
            if(sourceResource == null){
                // new resource shall be empty
                accessedData = new LinkedList<FileObject>();
            }else{
                // read the QName from the resourceEngager
                String accessData = IOUtils.toString(sourceResource.getDeployable(), "UTF-8");
                String[] dataArray = accessData.split(";");
                QName typeLoad = new QName(dataArray[0], dataArray[1]);

                // create new InputStream with the data
                InputStream newStream = IOUtils.toInputStream(accessData, "UTF-8");
                sourceResource = new RunnableContainerObject(sourceResource.getTargetModel(), newStream, sourceResource.getType());

                for (IDataLoader currentLoader : this.getDataLoader()) {
                    // check if IDataLoader is responsible for the prefix and forward the request
                    if (typeLoad.getNamespaceURI().equals(currentLoader.getSupportedNamespace("acquire").toString())) {
                        // load the desired resource
                        accessedData = currentLoader.getData(typeLoad, sourceResource);
                        
                        // special case docker containers: instance is saved as new image and so no targetResource is needed
                        if(typeLoad.getNamespaceURI().contains("mediawiki")||typeLoad.getNamespaceURI().contains("mysql")){
                            this.completeProcessExecutionSuccessfully(new URI(""), callback);
                            return;
                        }
                        break;
                    }
                } 
            } 
            
            // create new resource or finish execution if no targetResource is chosen
            if(targetResource != null){
                // read the QName from the second resourceEngager
                String accessData = IOUtils.toString(targetResource.getDeployable(), "UTF-8");
                String[] dataArray = accessData.split(";");
                QName typeStore = new QName(dataArray[0], dataArray[1]);

                // create new InputStream with the data
                InputStream newStreamStore = IOUtils.toInputStream(accessData, "UTF-8");
                targetResource = new RunnableContainerObject(targetResource.getTargetModel(), newStreamStore, targetResource.getType());
                
                // complete execution with error if data was not loaded successfully
                if (accessedData != null) {
                    String uri = null;
                    for (IDataStorage currentStorage : this.getDataStorage()) {
                        // check if IDataStorage is responsible for the prefix and forward the request
                        if (typeStore.getNamespaceURI().equals(currentStorage.getSupportedNamespace("acquire").toString())) {
                            // save the previously accessed resource in the desired location
                            uri = currentStorage.storeData(typeStore, targetResource, accessedData);
                            break;
                        }
                    }

                    // complete execution of the operation
                    if (uri != null) {
                        if(!uri.contains(";")){
                            // just the uri has to be returned
                            this.completeProcessExecutionSuccessfully(new URI(uri), callback);
                        }else{
                            // there are other informations about the new data resource to return
                            String[] info = uri.split(";");
                            this.completeProcessExecutionSuccessfullyWithAdditionalInformation(new URI(info[0]), info[1], callback);
                        }
                        
                    } else {
                        this.completeProcessExecutionwithError("No storage for saving the resource at the desired location found", callback);
                    }
                } else {
                    this.completeProcessExecutionwithError("No loader for accessing the desired resource found", callback);
                }
            }else{
                this.completeProcessExecutionwithError("A target resource RunnableContainer needs to be passed on every other resource than mediawiki and mysql.", callback); 
            }
        } catch (Exception e) {
            // execution failed due to an error
            this.completeProcessExecutionwithError(e.getMessage(), callback);
        }
    }

    /**
     * Complete the execution and pass an TOperationMessage object with the location and
     * the additional information to the callback.
     * 
     * @param resourceLocation The location of the created resource.
     * @param additionalInformation The additional information about the created resource.
     * @param callback The callback that has to be called with the informations.
     */
    private void completeProcessExecutionSuccessfullyWithAdditionalInformation(URI resourceLocation, String additionalInformation, OperationCallback callback) {
        TOtherParameters param = new TOtherParameters();
        param.setAny(additionalInformation);
        TOperationMessage outputParameters = new TOperationMessage();
        outputParameters.setInstanceLocation(resourceLocation.toString());
        outputParameters.setOtherParameters(param);
        outputParameters.setInstanceState(":engaged");
        callback.onSuccess(outputParameters); 
    }

    public List<URI> getSupportedDomains() {
        // operation is only possible if at least one storage exists
        if (this.getDataStorage().isEmpty()) {
            return null;
        }
        // iterate over every IDataLoader and add namespace
        List<URI> list = new LinkedList<URI>();
        for (IDataLoader currentLoader : this.getDataLoader()) {
            list.add(currentLoader.getSupportedNamespace("acquire"));
        }
        return list;
    }

    public List<QName> getSupportedResources() {
        // life-cycle operations have to be available for every data resource
        return null;
    }

    public List<QName> getRequiredDeployableTypes(QName resourceType) {
        List<QName> deployables = new ArrayList<QName>();
        int randomNumber = (int)(Math.random() * deployableAlternativesForFileResources.size()); 
        
        // mediawiki resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resource-instances/knowledge-resources/mediawiki") || resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/knowledge-resources/mediawiki")){
            deployables.add(resourceType);
        }
        
        // mysql resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/tables/mysql") || resourceType.getNamespaceURI().equals(this.getNamespace() + "resource-instances/data-resources/mysql") || resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/mysql")){
            deployables.add(resourceType);
        }
        
        // git resources
        if(resourceType.getNamespaceURI().startsWith(this.getNamespace() + "resources/data-resources/git")){
            deployables.add(deployableAlternativesForFileResources.get(randomNumber));
        }
        
        // file resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/file")){
            deployables.add(deployableAlternativesForFileResources.get(randomNumber));
        }
        
        // dropbox resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/dropbox")){
            deployables.add(deployableAlternativesForFileResources.get(randomNumber));
        }

        return deployables;
    }

}
