package uni_stuttgart.integration.eei;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;
import uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation;
import de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage;

/**
 * Class for execution of the release operation on resources.
 * The operation releases previously acquired data.
 * 
 * @author wederbn
 */
public class ReleaseInformationResources extends ReleaseResourceOperation{
    // list of available Data Loader for releasing data
    private List<IDataLoader> dataLoader;
    // context to load implementations of the IDataLoader interface
    private AnnotationConfigApplicationContext context = null;
    // the overall namespace
    private String namespace;
    // list of all QNames of alternatives to store file data
    private ArrayList<QName> deployableAlternativesForFileResources = new ArrayList<QName>();
    
    private String getNamespace() {
        return this.namespace;
    }

    private void setNamespace(String namespace) {
        this.namespace = namespace;
    }
    
    private List<IDataLoader> getDataLoader() {
        return this.dataLoader;
    }
    
    private void setDataLoader(List<IDataLoader> dataLoader) {
        this.dataLoader = dataLoader;
    }
    
    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }
    
    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context= annotationConfigApplicationContext;
    }
    
    /**
     * Create ReleaseResourceOperationInstance and load dependencies (IDataLoader)
     */
    public ReleaseInformationResources() {
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);
        
        // read namespace
        this.setNamespace(provider.getPropertyString("definitions.namespace"));
        
        // read package names
        String[] loaderPackages = provider.getPropertyStringArray("IDataLoader.locations");
        
        // scan desired packages for implementations of IDataLoader and add them to the list
        ArrayList<IDataLoader> loaderList = new ArrayList<IDataLoader>();
        for(String currentPackage : loaderPackages){
            try{
                this.setContext(new AnnotationConfigApplicationContext());
                this.getContext().scan(currentPackage);
                this.getContext().refresh();
                loaderList.addAll(this.getContext().getBeansOfType(IDataLoader.class).values());
            }catch(BeansException e){
                // package string was not valid
            }
        }
        this.setDataLoader(loaderList);
        
        // add deployable alternatives for file data
        deployableAlternativesForFileResources.add(new QName(this.getNamespace() + "resources/data-resources/dropbox", "integrating.dataresources@web.de/Empty Resource"));
        deployableAlternativesForFileResources.add(new QName(this.getNamespace() + "resources/data-resources/git/repository", "Integrating-Test/Empty Resource"));
    
    }
    
    public void executeOperation(List<RunnableContainer> resourceOrRelationshipContainer, Object inputParameters, OperationCallback callback) {
        try {
            // complete on Error if no or more than one RunnableContainer is passed
            if(resourceOrRelationshipContainer == null){
                this.completeProcessExecutionwithError(callback);
                return;
            }
            if(resourceOrRelationshipContainer.size() != 1){
                this.completeProcessExecutionwithError(callback); 
                return;
            }
            
            RunnableContainer resource = resourceOrRelationshipContainer.get(0);
            
            // read the QName from the resourceEngager
            String accessData = IOUtils.toString(resource.getDeployable(), "UTF-8");
            String[] dataArray = accessData.split(";");
            QName type = new QName(dataArray[0], dataArray[1]);
            
            // create new InputStream and RunnableContainer with the data
            InputStream newStream = IOUtils.toInputStream(accessData.split("\\|")[0], "UTF-8");
            RunnableContainer resourceEngager = new RunnableContainerObject(resource.getTargetModel(), newStream, resource.getType());
            
            boolean loaderFound = false;
            for (IDataLoader currentLoader : this.getDataLoader()) {
                // check if IDataLoader is responsible for the prefix and forward the request
                if (type.getNamespaceURI().equals(currentLoader.getSupportedNamespace("release").toString())) {
                    currentLoader.releaseData(type, resourceEngager);
                    loaderFound = true;
                    break;
                }
            }
            
            // complete execution of the operation
            if (loaderFound) {
                this.completeProcessExecutionSuccessfully(callback);
            } else {
                this.completeProcessExecutionwithError(callback);
            }
        } catch (Exception e) {
            this.completeProcessExecutionwithError(callback);
        }
    }
    
    public List<URI> getSupportedDomains() {
        // iterate over every IDataLoader and add namespace
        List<URI> list = new LinkedList<URI>();
        for(IDataLoader currentLoader : this.getDataLoader()){
            list.add(currentLoader.getSupportedNamespace("release"));
        }
        return list;
    }
    
    public List<QName> getSupportedResources() {
        // life-cycle operations have to be available for every data resource
        return null;
    }

    public void completeProcessExecutionSuccessfully(OperationCallback callback) {
        TOperationMessage outputParameters = new TOperationMessage();
        outputParameters.setInstanceState(":engaged");
        callback.onSuccess(outputParameters);
    }

    public void completeProcessExecutionwithError(OperationCallback callback) {
        TOperationMessage outputParameters = new TOperationMessage();
        outputParameters.setInstanceState(":erroronous");
        callback.onError(outputParameters);
    }

    public List<QName> getRequiredDeployableTypes(QName resourceType) {
        List<QName> deployables = new ArrayList<QName>();
        int randomNumber = (int)(Math.random() * deployableAlternativesForFileResources.size()); 
        
        // mediawiki resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resource-instances/knowledge-resources/mediawiki") || resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/knowledge-resources/mediawiki")){
            deployables.add(resourceType);
        }
        
        // mysql resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/tables/mysql") || resourceType.getNamespaceURI().equals(this.getNamespace() + "resource-instances/data-resources/mysql") || resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/mysql")){
            deployables.add(resourceType);
        }
        
        // git resources
        if(resourceType.getNamespaceURI().startsWith(this.getNamespace() + "resources/data-resources/git")){
            deployables.add(deployableAlternativesForFileResources.get(randomNumber));
        }
        
        // file resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/file")){
            deployables.add(deployableAlternativesForFileResources.get(randomNumber));
        }
        
        // dropbox resources
        if(resourceType.getNamespaceURI().equals(this.getNamespace() + "resources/data-resources/dropbox")){
            deployables.add(deployableAlternativesForFileResources.get(randomNumber));
        }

        return deployables;
    }
    
}
