package uni_stuttgart.integration.eei;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.w3c.dom.Element;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerCertificates;

import de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage;
import de.uni_stuttgart.iaas.ipsm.v0.TOtherParameters;
import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;
import uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireRelationshipOperation;

/**
 * Class for the execution of the acquire operation of the admin relationship.
 * 
 * @author wederbn
 */
public class AcquireAdminRelationshipOperation extends AcquireRelationshipOperation{

    // context to load implementations of the IDataLoader and IDataStorage interfaces
    private AnnotationConfigApplicationContext context = null;
    // the namespace this class is used for
    private String supportedNamespace;

    private AnnotationConfigApplicationContext getContext() {
        return this.context;
    }
    
    private void setContext(AnnotationConfigApplicationContext annotationConfigApplicationContext) {
        this.context = annotationConfigApplicationContext;
    }
    
    /**
     * Create AcquireRlationshipOperationInstance and load dependencies (IRelationshipLoader).
     * @throws URISyntaxException 
     */
    public AcquireAdminRelationshipOperation(){
        // load configuration provider
        this.setContext(new AnnotationConfigApplicationContext());
        this.getContext().scan("uni_stuttgart.integration.configuration_files");
        this.getContext().refresh();
        IConfigurationProvider provider = this.getContext().getBean(IConfigurationProvider.class);
        // read overall namespace prefix
        this.supportedNamespace = provider.getPropertyString("definitions.namespace");
    }
    
    public void executeOperation(List<RunnableContainer> resourceOrRelationshipContainer, Object inputParameters, OperationCallback callback) {
        try {
            // complete on Error if no RunnableContainers are passed
            if(resourceOrRelationshipContainer == null || resourceOrRelationshipContainer.size() < 1){
                this.completeProcessExecutionwithError("Acquire admin relationship needs a RunnableContainer of the relationship!", callback);
                return;
            }
            
            // read the needed data from the sourceResourceEngager
            String accessData = IOUtils.toString(resourceOrRelationshipContainer.get(0).getDeployable(), "UTF-8");
            String[] temp = accessData.split(";");
            
            if (temp.length < 4) {
                throw new Exception("Data from sourceResourceEngager was not valid. Maybe no resourceEngager of the admin relationship.");
            } else {
                // create docker client to find container id
                DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
                DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
     
                // list all container and search for the mediawiki instance
                String id = null;
                List<Container> contList = dockerClient.listContainersCmd().exec();
                String[] localParts = temp[1].split("/");
                for(Container cont : contList){
                    if(cont.getNames()[0].substring(1).equals(localParts[localParts.length-1])){
                        id = cont.getId();
                        break;
                    }
                }
                
                if(id == null){
                    throw new Exception("Mediawiki container for the relationship not found.");
                }
                
                String dockerIP = temp[2].substring(6).split(":")[0]; // cut out ip address
                String dockerPort = temp[2].substring(6).split(":")[1]; // cut out port
                
                // load data from NodeTemplate of human user attributes
                String mediawikiAdmin = null, mediawikiAdminPass = null;
                TEntityTemplate template = resourceOrRelationshipContainer.get(0).getTargetModel();
                Properties properties = template.getProperties();
                if(properties != null){
                    Element element = (Element) properties.getAny();
                    if(element.hasAttribute("userName")){
                        mediawikiAdmin = element.getAttribute("userName");
                    }
                    if(element.hasAttribute("password")){
                        mediawikiAdminPass = element.getAttribute("password");
                    }
                }
                
                if(mediawikiAdmin == null || mediawikiAdminPass == null){
                    throw new Exception("'userName' or 'password' attribute is not set for the human user.");
                }
                
                // create connection to the mediawiki instance that is the source resource
                final com.spotify.docker.client.DockerClient docker = DefaultDockerClient.builder()
                        .uri(URI.create("https://" + dockerIP + ":" + dockerPort))
                        .dockerCertificates(new DockerCertificates(Paths.get(temp[3])))
                        .build();
                // run createAndPromote.php script to add the human resource as new administrator
                final String[] command = {"php", "/var/www/html/maintenance/createAndPromote.php"
                        , "--sysop", mediawikiAdmin, mediawikiAdminPass};
                final String execId = docker.execCreate(
                        id, command, com.spotify.docker.client.DockerClient.ExecCreateParam.attachStdout(),
                        com.spotify.docker.client.DockerClient.ExecCreateParam.attachStderr());
                docker.execStart(execId);
                docker.close();
                
                // return information on the established relationship
                String information = "Administrator relationship between user and mediawiki successfully acquired. \n";
                information += "Mediawiki instance: http://" + dockerIP + ":" + Helper.findPort(temp[2], temp[3], localParts[localParts.length-1], 80) + "\n";
                information += "Selected user name: " + mediawikiAdmin + "\n";
                information += "Selected password: " + mediawikiAdminPass;
                this.completeProcessExecutionSuccessfully(information, callback);
            }
        }catch(Exception e){
            this.completeProcessExecutionwithError("An error occured during the creation of the relationship. " + e.getMessage(), callback); 
            return;  
        }
    }
    
    /**
     * Complete the execution on success and pass information to callback
     */
    private void completeProcessExecutionSuccessfully(String information, OperationCallback callback) {
        TOtherParameters param = new TOtherParameters();
        param.setAny(information);
        TOperationMessage outputParameters = new TOperationMessage();
        outputParameters.setOtherParameters(param);
        outputParameters.setInstanceState(":engaged");
        callback.onSuccess(outputParameters); 
    }

    /**
     * Complete the execution on error and pass information to callback
     */
    private void completeProcessExecutionwithError(String errorDescription, OperationCallback callback) {
        TOperationMessage outputParameters = new TOperationMessage();
        outputParameters.setErrorDefinition(errorDescription);
        outputParameters.setInstanceState(":erroronous");
        callback.onError(outputParameters); 
        
    }

    public List<URI> getSupportedTargetRelationships() {
        return null;
    }
    
    public List<URI> getSupportedSourceRelationships() {
        return null;
    }
    
    public List<URI> getSupportedTargetDomains() {
        List<URI> uriList = new ArrayList<URI>();
        try {
            // only human resources are supported as target
            uriList.add(new URI(this.supportedNamespace + "human-resource"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uriList;
    }
    
    public List<URI> getSupportedSourceDomains() {
        List<URI> uriList = new ArrayList<URI>();
        try {
            // mediawiki instances are supported as source
            uriList.add(new URI(this.supportedNamespace + "resources/knowledge-resources/mediawiki"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uriList;
    }

    public List<QName> getRequiredDeployableTypes(QName resourceType) {
        List<QName> nameList = new ArrayList<QName>();
        nameList.add(resourceType);
        return nameList;
    }    
}
