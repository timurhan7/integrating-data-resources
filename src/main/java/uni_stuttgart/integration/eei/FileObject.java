package uni_stuttgart.integration.eei;

import java.io.InputStream;

/**
 * This class represents one file that is contained in any data resource. It
 * contains the file as InputStream, the name of the file and the local path of
 * the file in the data resource, to allow the restoration of the file
 * hierarchy.
 *
 * @author wederbn
 */
public class FileObject {

    private String name;
    private String localPath;
    private long size;
    private InputStream stream;
    
    
    /**
     * Creates an FileObject
     *
     * @param name The name of the represented file.
     * @param localPath The localPath in the data resource. If the file is
     *            placed directly (without directories in between) in the data
     *            resource the argument can be null.
     * @param size The size of the file
     * @param stream The stream containing the data of the file
     */
    public FileObject(String name, String localPath, long size, InputStream stream) {
        this.name = name;
        this.size = size;
        this.stream = stream;
        if (localPath == null) {
            this.localPath = "";
        } else {
            this.localPath = localPath;
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLocalPath() {
        return this.localPath;
    }
    
    public InputStream getStream() {
        return this.stream;
    }
    
    public long getSize() {
        return this.size;
    }
    
}
