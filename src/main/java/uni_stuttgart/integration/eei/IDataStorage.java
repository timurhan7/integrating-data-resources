package uni_stuttgart.integration.eei;

import java.net.URI;
import java.util.List;

import javax.xml.namespace.QName;

import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * This interface needs to be implemented to add a new data source.
 * The data source specific code for storing data is hidden behind this interface.
 * @author wederbn
 *
 */
public interface IDataStorage {

    /**
     * Stores a data resource in the data source represented by this object.
     *
     * @param type The type of the resource that shall be stored in the data
     *            source represented by this object. The resource must be an
     *            empty resource.
     * @param resourceEngager Contains all information needed to create the
     *            resource and store the data in it.
     * @param files The files that shall be stored in the data resource (as
     *            FileObjects).
     * @return The URI of the stored resource instance plus other needed informations
     * (like passwords) if needed (format: URI;other informations).
     * @throws Exception is thrown if the data was not stored correctly
     */
    String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception;

    /**
     * Returns the namespace of the data source represented by this object
     *
     * @param operation The kind of operation that uses the {@link URI}.
     * @return The namespace supported by this object contained in a {@link URI}
     *         .
     */
    URI getSupportedNamespace(String operation);
}
