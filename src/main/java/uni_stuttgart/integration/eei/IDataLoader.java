package uni_stuttgart.integration.eei;

import java.io.InputStream;
import java.net.URI;
import java.util.List;

import javax.xml.namespace.QName;

import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * This interface needs to be implemented to add a new data source.
 * The data source specific code for loading and releasing data is hidden behind this interface.
 * @author wederbn
 *
 */
public interface IDataLoader {

    /**
     * Provide a data resource of the data source represented by this object.
     *
     * @param type The {@link QName} of the desired resource
     * @param resourceEngager The {@link RunnableContainer} that contains all
     *            information to load the desired resource.
     * @return A list of {@link InputStream}s containing the desired data
     *         resource.
     * @throws Exception is thrown if the data was not accessed correctly
     */
    List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception;
    
    /**
     * Release a data resource of the data source represented by this object.
     *
     * @param type The {@link QName} of the desired resource
     * @param resourceEngager The {@link RunnableContainer} that contains all
     *            information to release the desired resource.
     * @throws Exception is thrown if the release of the data resource fails
     */
    void releaseData(QName type, RunnableContainer resourceEngager) throws Exception;
    
    /**
     * Returns the namespace of the data source represented by this object
     * 
     * @param operation The kind of operation that uses the {@link URI}.
     * @return The namespace supported by this object contained in a {@link URI}
     *         .
     */
    URI getSupportedNamespace(String operation);
}
