package uni_stuttgart.integration.eei.implementations.knowledge_resources.mediawiki;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for creation of new database images to enable the
 * recreation of stored mediawiki instances. The class is
 * also concerned for releasing mediawiki instances and the
 * related databases.
 * 
 * @author wederbn
 *
 */
@Service
public class MediawikiDataLoader implements IDataLoader{
    
    // the namespace this class is used for
    private String supportedNamespace;
    
    public MediawikiDataLoader() throws URISyntaxException{
        // read namespace prefix and create QName with that prefix and the mysql uri
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = provider.getPropertyString("definitions.namespace");
        context.close();
    }

    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the loading
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            // create docker client related to the resource
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            
            // create random name for new image tag and overwrite if name is set
            String mediawikiName = Helper.createRandomName(8);
            TEntityTemplate template = resourceEngager.getTargetModel();
            Properties properties = template.getProperties();
            if(properties != null){
                Element element = (Element) properties.getAny();
                if(element.hasAttribute("mediawikiName")){
                    mediawikiName = element.getAttribute("mediawikiName");
                }
            }
            
            // list all container and search for the mediawiki instance
            String dbId = null;
            List<Container> contList = dockerClient.listContainersCmd().exec();
            String[] localParts = temp[1].split("/");
            for(Container cont : contList){
                if(cont.getNames()[0].substring(1).equals(localParts[localParts.length-1])){
                    // search for the related databaseID
                    dbId = cont.getLabels().get("databaseID");
                    break;
                }
            }
            
            // list all container and commit related db container as new image
            for(Container cont : contList){
                if(cont.getId().equals(dbId)){
                    dockerClient.commitCmd(cont.getId()).withRepository("appcontainers/mysql").withTag(mediawikiName).exec();
                    break;
                }
            }
        }
        // no return value needed because images are saved internally 
        return null;
    }

    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            // create docker client related to this resource
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            
            // iterate over all containers in the docker client
            List<Container> containers = dockerClient.listContainersCmd().exec();
            String[] localParts = temp[1].split("/");
            for(Container container : containers){
                // stop and remove the container that contains the resource and the related database
                if(container.getNames()[0].substring(1).equals(localParts[localParts.length-1])){
                    String dbId = container.getLabels().get("databaseID");
                    dockerClient.stopContainerCmd(container.getId()).exec();
                    dockerClient.removeContainerCmd(container.getId()).exec();
                    dockerClient.stopContainerCmd(dbId).exec();
                    dockerClient.removeContainerCmd(dbId).exec();
                }
            }
        } 
    }

    public URI getSupportedNamespace(String operation) {
        try {
            // images are just supported by acquire and instances just by store and release
            if(operation.equals("acquire")){
                return new URI(this.supportedNamespace + "resources/knowledge-resources/mediawiki"); 
            }else{
                return new URI(this.supportedNamespace + "resource-instances/knowledge-resources/mediawiki");
            }    
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

}
