package uni_stuttgart.integration.eei.implementations.knowledge_resources.mediawiki;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.PullImageResultCallback;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerCertificates;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataStorage;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for creation of new mediawiki instances. 
 * Enables to create a new instance with a new database or to create a mediawiki
 * instance that uses an existing database.
 * 
 * @author wederbn
 *
 */
@Service
public class MediawikiDataStorage implements IDataStorage{
    
    // the namespace this class is used for
    private String supportedNamespace;
    
    public MediawikiDataStorage() throws URISyntaxException{
        // read namespace prefix and create QName with that prefix and the mysql uri
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = provider.getPropertyString("definitions.namespace");
        context.close();
    }

    public String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (!type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Storing data needs a RunnableContainer of an empty resource as input");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try{
                // create docker client related to this empty resource
                DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
                DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
                String dockerIP = temp[2].substring(6).split(":")[0]; // cut out ip address
                String dockerPort = temp[2].substring(6).split(":")[1]; // cut out port
                
                // pull the needed Image if not already done
                dockerClient.pullImageCmd("appcontainers/mysql:latest").exec(new PullImageResultCallback()).awaitSuccess();
                dockerClient.pullImageCmd("synctree/mediawiki:latest").exec(new PullImageResultCallback()).awaitSuccess();

                // load data from NodeTemplate attributes
                String mediawikiName = null, mediawikiAdmin = null, mediawikiAdminPass = null;
                TEntityTemplate template = resourceEngager.getTargetModel();
                Properties properties = template.getProperties();
                if(properties != null){
                    Element element = (Element) properties.getAny();
                    if(element.hasAttribute("mediawikiName")){
                        mediawikiName = element.getAttribute("mediawikiName");
                    }
                    if(element.hasAttribute("mediawikiAdmin")){
                        mediawikiAdmin = element.getAttribute("mediawikiAdmin");
                    }
                    if(element.hasAttribute("mediawikiAdminPass")){
                        mediawikiAdminPass = element.getAttribute("mediawikiAdminPass");
                    }
                }
                
                // set default name, admin and password if no attribute was passed
                if(mediawikiName == null){
                    mediawikiName = Helper.createRandomName(8);  
                }
                if(mediawikiAdmin == null){
                    mediawikiAdmin = "admin";
                }
                if(mediawikiAdminPass == null){
                    mediawikiAdminPass = "password";
                }
                
                // search for right database image to create empty mediawiki or previously stored version
                String[] namespaceParts = temp[1].split("/");   // 0: docker uri; 1: docker image; 2: name
                String imageName = namespaceParts[1];
                if(imageName.equals("newMediawiki")){
                    imageName = "";
                }else{
                    imageName = ":" + imageName;
                }
   
                // create environment variables for the mysql database
                String dbContainerName = Helper.createRandomName(8); // generate random mysql container name
                String dbName = "mediawiki";
                String dbUser = "admin";
                String dbPass = "password";
                    
                // create and start a mysql database container for the new mediawiki instance
                ExposedPort tcpPort = ExposedPort.tcp(3306);
                Ports portBindings = new Ports();
                portBindings.bind(tcpPort, Ports.binding(null)); // map to random port
                CreateContainerResponse container = dockerClient.createContainerCmd("appcontainers/mysql" + imageName)
                        .withExposedPorts(tcpPort)
                        .withPortBindings(portBindings)
                        .withName(dbContainerName)
                        .withHostName("mysql")
                        .withEnv("CREATEDB=true", "MYSQL_CLIENT=%", "MYSQL_PASS=PAssw0rd", "MYSQL_DB=" + dbName, "APP_USER=" + dbUser, "APP_PASS=" + dbPass)
                        .exec();
                dockerClient.startContainerCmd(container.getId()).exec();
                    
                // find port of the mysql instance
                String mySqlPort = Helper.findPort(temp[2], temp[3], dbContainerName, 3306);
                String databaseURI = dockerIP + ":" + mySqlPort;
                
                // create Map to store the associated database
                Map<String, String> databaseMap = new HashMap<String, String>();
                databaseMap.put("databaseID", container.getId());
                
                // create and start the mediawiki container
                ExposedPort tcpPortWiki = ExposedPort.tcp(80);
                Ports portBindingsWiki = new Ports();
                portBindingsWiki.bind(tcpPortWiki, Ports.binding(null));
                CreateContainerResponse containerWiki = dockerClient.createContainerCmd("synctree/mediawiki")
                        .withExposedPorts(tcpPortWiki)
                        .withPortBindings(portBindingsWiki)
                        .withLabels(databaseMap)
                        .withName(mediawikiName)
                        .withEnv("MEDIAWIKI_DB_HOST=" + databaseURI, "MEDIAWIKI_DB_NAME=" + dbName, "MEDIAWIKI_DB_USER=" + dbUser, "MEDIAWIKI_DB_PASSWORD=" + dbPass)
                        .exec();
                dockerClient.startContainerCmd(containerWiki.getId()).exec();   
                
                // find port of the mediawiki instance
                String mediawikiPort = Helper.findPort(temp[2], temp[3], mediawikiName, 80);
                
                // wait until the database is ready and responds
                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://" + databaseURI + "/" + dbName;
                boolean noConnection = true;
                while(noConnection){
                    try{
                        // try to connect to the database
                        DriverManager.getConnection(url, dbUser, dbPass);
                        noConnection = false;
                    }catch(Exception e){
                        // wait 1 seconds and retry
                        Thread.sleep(1000);
                    } 
                }
                
                // wait until the installation progress is finished
                Thread.sleep(10000);
                
                // run install.php script to initialize the instance
                final com.spotify.docker.client.DockerClient docker = DefaultDockerClient.builder()
                        .uri(URI.create("https://" + dockerIP + ":" + dockerPort))
                        .dockerCertificates(new DockerCertificates(Paths.get(temp[3])))
                        .build();
                final String[] command = {"php", "/var/www/html/maintenance/install.php"
                        , "--dbname",  dbName, "--dbserver", databaseURI 
                        , "--dbuser", dbUser, "--dbpass", dbPass, "--installuser", dbUser
                        , "--installpass", dbPass, "--pass", mediawikiAdminPass, mediawikiName, mediawikiAdmin};
                final String execId = docker.execCreate(
                        containerWiki.getId(), command, com.spotify.docker.client.DockerClient.ExecCreateParam.attachStdout(),
                        com.spotify.docker.client.DockerClient.ExecCreateParam.attachStderr());
                docker.execStart(execId);

                //create LocalSettings.php file to configure the new instance
                FileUtils.writeStringToFile(new File("./" + "LocalSettings.php"), createLocalSettingsPHP(mediawikiName, databaseURI, dbName, dbUser, dbPass));
                
                // wait for the initialization of the wiki
                Thread.sleep(4000);
                
                // copy LocalSettings.php into container
                dockerClient.copyArchiveToContainerCmd(containerWiki.getId()).withRemotePath("/var/www/html/").withHostResource("./" + "LocalSettings.php").exec();
                
                // run createAndPromote.php script to add new administrator
                final String[] command2 = {"php", "/var/www/html/maintenance/createAndPromote.php"
                        , "--sysop", "--bureaucrat", mediawikiAdmin, mediawikiAdminPass};
                final String execId2 = docker.execCreate(
                        containerWiki.getId(), command2, com.spotify.docker.client.DockerClient.ExecCreateParam.attachStdout(),
                        com.spotify.docker.client.DockerClient.ExecCreateParam.attachStderr());
                docker.execStart(execId2);
                docker.close();
                
                return "http://" + dockerIP + ":" + mediawikiPort; 
                
            }catch(Exception e){
                throw new Exception("Creation of mediawiki instance not possible: " + e.getMessage());
            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        try {
            // images are just supported by acquire and instances just by store and release
            if(operation.equals("acquire")){
                return new URI(this.supportedNamespace + "resources/knowledge-resources/mediawiki"); 
            }else{
                return new URI(this.supportedNamespace + "resource-instances/knowledge-resources/mediawiki");
            }    
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Creates the LocalSettings.php file content with the parameter values.
     * 
     * @return The content of the LocalSettings.php file
     */
    private String createLocalSettingsPHP(String siteName, String dbServer, String dbName, String dbUser, String dbPass){
        String inhalt = "<?php " 
                //global settings
                + "$wgSitename = \"" + siteName + "\";"
                + "$wgScriptPath = \"\";"
                + "$wgResourceBasePath = $wgScriptPath;"
                + "$wgLogo = \"$wgResourceBasePath/resources/assets/wiki.png\";"
                //database settings
                + "$wgDBtype = \"mysql\";"
                + "$wgDBserver = \"" + dbServer + "\";"
                + "$wgDBname = \"" + dbName + "\";"
                + "$wgDBuser = \"" + dbUser + "\";"
                + "$wgDBpassword = \""+ dbPass + "\";"
                //skin settings
                + "$wgDefaultSkin = \"vector\";"
                + "require_once \"$IP/skins/CologneBlue/CologneBlue.php\";"
                + "require_once \"$IP/skins/Modern/Modern.php\";"
                + "require_once \"$IP/skins/MonoBook/MonoBook.php\";"
                + "require_once \"$IP/skins/Vector/Vector.php\";";
        return inhalt;      
    }

}
