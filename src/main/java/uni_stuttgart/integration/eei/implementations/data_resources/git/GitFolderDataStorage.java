package uni_stuttgart.integration.eei.implementations.data_resources.git;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataStorage;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of git folders as data source
 * 
 * @author wederbn
 *
 */
@Service
public class GitFolderDataStorage implements IDataStorage{
    
    // the namespace this class is used for
    private URI supportedNamespace;
    // local path for cloning repositories
    private static final String localPath = "./Repos/";


    /**
     * Create a GitFolderDataStorage and load the namespace prefix of the
     * application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public GitFolderDataStorage() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/git/folder");
        context.close();
    }
    
    public String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (!type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Storing data needs a RunnableContainer of an empty resource as input");
        }

        if(files == null || files.size()<1){
            throw new Exception("Creation of an new folder without data to store is not possible. Git only lists folders with files in it.");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 5) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            File file;
            try{
                // access the desired data source
                String localPart = temp[2].substring(7); //cut off https:/ (change if git: is used)
                file = new File(GitFolderDataStorage.localPath + localPart);
                Helper.cloneRepository(temp[2], temp[3], temp[4], file);
            } catch (Exception e) {
                throw new Exception("Cloning of the target repository not possible");
            }

            try {
                // get name of the new data resource
                String name;
                TEntityTemplate template = resourceEngager.getTargetModel();
                Properties properties = template.getProperties();
                if(properties != null){
                    Element element = (Element) properties.getAny();
                    if(element.hasAttribute("name")){
                        name = element.getAttribute("name");
                    }else{
                        name = Helper.createRandomName(8);  
                    }
                }else{
                    name = Helper.createRandomName(8);  
                }
                
                // get path to new resource
                File desiredResource = new File(file.getPath() + "/" + name);
                if (desiredResource.exists()) {
                    // not able to create resource if file with that name already exists
                    throw new Exception("Name is already in use");
                }
                List<String> newFilePaths = Helper.createFiles(desiredResource, files);
                
                // search local repository
                FileRepositoryBuilder builder = new FileRepositoryBuilder();
                Repository repository = builder.setGitDir(new File(file.getAbsolutePath() + File.separator + ".git")).readEnvironment().findGitDir().build();
                Git git = new Git(repository);

                // add all new files to the index
                for (String path : newFilePaths) {
                    path = path.substring(desiredResource.getAbsolutePath().length() + 1);
                    git.add().addFilepattern(name + "/" + path).call();
                }
                
                // commit and push the changes
                git.commit().setMessage("Stored data resource: " + name).call();
                git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(temp[3], temp[4])).call();
                git.close();
                
                // return uri
                String prefix = temp[2].substring(0, temp[2].length() - 4); // cut of .git
                return prefix + "/tree/master/" + name;
            } catch (Exception e) {
                throw new Exception("Failure: " + e.getMessage());
            }
        }
    }
    
    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }
}
