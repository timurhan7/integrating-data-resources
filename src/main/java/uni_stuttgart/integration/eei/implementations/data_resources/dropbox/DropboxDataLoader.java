package uni_stuttgart.integration.eei.implementations.data_resources.dropbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v1.DbxClientV1;
import com.dropbox.core.v1.DbxEntry;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.integration.eei.implementations.data_resources.file.DeleteOnCloseFileInputStream;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of dropbox folders as data source
 *
 * @author wederbn
 *
 */

@Service
public class DropboxDataLoader implements IDataLoader{
    
    // the namespace this class is used for
    private URI supportedNamespace;
    
    /**
     * Create a DropboxDataLoader and load the namespace prefix of the application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public DropboxDataLoader() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/dropbox");
        context.close();
    }

    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");

        // check if all needed data is available
        if (temp.length < 5) {
            throw new Exception("Data from resourceEngager was not valid");
        }else{
            try{
                // create dropbox client
                DbxRequestConfig config = new DbxRequestConfig("", Locale.getDefault().toString());
                DbxClientV1 client = new DbxClientV1(config, temp[2]);
                
                return getFilesInFolderRecursive(temp[1].substring(temp[1].indexOf("/")), client);
            }catch(Exception e){
                throw new Exception("Unable to load data from dropbox. " + e.getMessage());
            }
        }
    }

    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }
        

        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");

        // check if all needed data is available
        if (temp.length < 5) {
            throw new Exception("Data from resourceEngager was not valid");
        }else{
            try{
                // create dropbox client
                DbxRequestConfig config = new DbxRequestConfig("", Locale.getDefault().toString());
                DbxClientV1 client = new DbxClientV1(config, temp[2]);
                
                // delete resource
                client.delete(temp[1].substring(temp[1].indexOf("/")));
            }catch(Exception e){
                throw new Exception("Unable to delete resource. " + e.getMessage());
            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        return supportedNamespace;
    }
    
    /**
     * Get FileObjects of all files contained in a dropbox folder.
     * 
     * @param path The path of the desired folder.
     * @param client The client through which the folder can be accessed
     * @return A list of FileObjects from files in the folder.
     * @throws Exception
     */
    private List<FileObject> getFilesInFolderRecursive(String path, DbxClientV1 client) throws Exception{
        List<FileObject> files = new ArrayList<FileObject>();
        // get all children of the path
        DbxEntry.WithChildren listing = client.getMetadataWithChildren(path);
        if(!path.endsWith("/")){
            path = path + "/"; 
        }
        for(DbxEntry child : listing.children){
            // search recursive for further files
            if(child.isFolder()){
                files.addAll(getFilesInFolderRecursive(path + child.name, client));
            }else{
                // add the file to the list
                String localPath = path.substring(1);
                File temp = new File("./DropboxTemp/" + child.name);
                temp.getParentFile().mkdirs();
                temp.getParentFile().setWritable(true);
                OutputStream out = new FileOutputStream(temp);
                client.getFile(path + child.name, null, out);
                out.close();
                // create stream that deletes the file onClose to avoid garbage
                FileInputStream in = new DeleteOnCloseFileInputStream(temp);
                FileObject file = new FileObject(child.name, localPath.substring(localPath.indexOf("/") + 1), temp.length(), in);
                files.add(file);
            }
        }
        return files;
    }

}
