package uni_stuttgart.integration.eei.implementations.data_resources.file;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of local folders as data source
 *
 * @author wederbn
 *
 */

@Service
public class FileDataLoader implements IDataLoader {

    // the namespace this class is used for
    private URI supportedNamespace;


    /**
     * Create a FileDataLoader and load the namespace prefix of the application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public FileDataLoader() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/file");
        context.close();
    }

    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        if (temp.length < 3) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try {
                // access all contained files and create InputStreams for them
                File desiredResource = new File(temp[2]);
                List<String> paths = Helper.listLocalPaths(desiredResource);
                return Helper.createInputStreams(paths, desiredResource);
            } catch (Exception e) {
                throw new Exception("Accessing files not possible");
            }
        }
    }

    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }

        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");

        // check if all needed data is available
        if (temp.length < 3) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try {
                // delete the desired resource and all contained files
                File desiredResource = new File(temp[2]);
                Helper.deleteDirectory(desiredResource);
            } catch (Exception e) {
                throw new Exception("Failure during release of data");
            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }

}
