package uni_stuttgart.integration.eei.implementations.data_resources.file;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataStorage;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of local folders as data source
 * 
 * @author wederbn
 *
 */

@Service
public class FileDataStorage implements IDataStorage {

    // the namespace this class is used for
    private URI supportedNamespace;
    
    
    /**
     * Create a FileDataStorage and load the namespace prefix of the application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public FileDataStorage() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/file");
        context.close();
    }

    public String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (!type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Storing data needs a RunnableContainer of an empty resource as input");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 3) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try {
                // get name of the new data resource (from TargetModel if specified or random name otherwise)
                String name;
                TEntityTemplate template = resourceEngager.getTargetModel();
                Properties properties = template.getProperties();
                if(properties != null){
                    Element element = (Element) properties.getAny();
                    if(element.hasAttribute("name")){
                        name = element.getAttribute("name");
                    }else{
                        name = Helper.createRandomName(8);  
                    }
                }else{
                    name = Helper.createRandomName(8);  
                }
                
                // create the needed files
                File desiredResource = new File(temp[2] + name);              
                if (desiredResource.exists()) {
                    // not able to create resource if file with that name already exists
                    throw new Exception("Name is already in use");
                }
                desiredResource.mkdirs();
                desiredResource.setWritable(true);
                Helper.createFiles(desiredResource, files);
                
                // create correct path form for the file:// protocol
                String path = desiredResource.getAbsolutePath().replace(File.separatorChar, '/');
                return "file://" + path.replaceAll(" ", "%20"); // replace spaces for usage of URI
            } catch (Exception e) {
                throw new Exception("Storing of the data failed!" + e.getMessage());
            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }
    
}
