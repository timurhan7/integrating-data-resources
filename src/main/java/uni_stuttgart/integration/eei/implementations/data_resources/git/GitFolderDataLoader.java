package uni_stuttgart.integration.eei.implementations.data_resources.git;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of git folders as data source
 * 
 * @author wederbn
 *
 */
@Service
public class GitFolderDataLoader implements IDataLoader{
    
    // the namespace this class is used for
    private URI supportedNamespace;
    // local path for cloning repositories
    private static final String localPath = "./Repos/";
    
    
    /**
     * Create a GitFolderDataLoader and load the namespace prefix of the
     * application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public GitFolderDataLoader() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/git/folder");
        context.close();
    }
    
    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the loading
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }

        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 5) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try {
                // access the desired data source
                String localPart = temp[2].substring(7); //cut off https:/ (change if git: is used)
                File file = new File(GitFolderDataLoader.localPath + localPart);
                Helper.cloneRepository(temp[2], temp[3], temp[4], file);
                // access the files in the data resource and create InputStreams
                File desiredResource = new File(GitFolderDataLoader.localPath + "/" + type.getLocalPart());
                List<String> paths = Helper.listLocalPaths(desiredResource);
                return Helper.createInputStreams(paths, desiredResource);
            } catch (Exception e) {
                e.printStackTrace();
                // loading data failed --> Operation has to call onError() on Callback
                throw new Exception("Cloning of the repository failed");
            }
        }
    }
    
    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 5) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try {
                // access the desired data source
                String localPart = temp[2].substring(7); //cut off https:/ (change if git: is used)
                File file = new File(GitFolderDataLoader.localPath + localPart);
                Helper.cloneRepository(temp[2], temp[3], temp[4], file);
                File desiredResource = new File(GitFolderDataLoader.localPath + "/" + type.getLocalPart());
                
                // get paths of the files in the data resource
                List<String> paths = Helper.listLocalPaths(desiredResource);

                // search local repository
                FileRepositoryBuilder builder = new FileRepositoryBuilder();
                Repository repository = builder.setGitDir(new File(file.getAbsolutePath() + File.separator + ".git")).readEnvironment().findGitDir().build();
                Git git = new Git(repository);

                // add all paths to the remove index
                String[] localParts = type.getLocalPart().split("/");
                String name = localParts[localParts.length-1];
                for (String path : paths) {
                    path = path.substring(desiredResource.getAbsolutePath().length() + 1);
                    path = path.replace(File.separatorChar, '/');
                    git.rm().addFilepattern(name + "/" + path).call();
                }

                // commit and push the changes
                git.commit().setMessage("Deleted data resource: " + name).call();
                git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(temp[3], temp[4])).call();
                git.close();

                // delete local files
                Helper.deleteDirectory(desiredResource);
            } catch (Exception e) {
                e.printStackTrace();
                throw new Exception("Failure during release of data");
            }
        }
    }
    
    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }
    
}
