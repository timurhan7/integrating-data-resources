package uni_stuttgart.integration.eei.implementations.data_resources.git;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of git repositories as data source
 * 
 * @author wederbn
 *
 */

@Service
public class GitRepositoryDataLoader implements IDataLoader {

    // the namespace this class is used for
    private URI supportedNamespace;
    // local path for cloning repositories
    private static final String localPath = "./Repos/";
    
    
    /**
     * Create a GitRepositoryDataLoader and load the namespace prefix of the
     * application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public GitRepositoryDataLoader() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/git/repository");
        context.close();
    }

    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the loading
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");

        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            // search desired repository in the list of all repositories of the user
            String url = null;
            // use authenticated requests to get a higher API request limit
            GitHubClient client = new GitHubClient();
            client.setCredentials(temp[2], temp[3]);
            RepositoryService service = new RepositoryService(client);
            String[] localParts = temp[1].split("/");
            for (org.eclipse.egit.github.core.Repository repo : service.getRepositories(temp[2])) {
                // compare local part of the QName of the desired resource with the repo name
                if (repo.getName().equals(localParts[localParts.length-1])) {
                    url = repo.getCloneUrl();
                    break;
                }
            }

            // abort if repository is not found (was deleted between the two requests (listDomain, getData))
            if (url == null) {
                throw new Exception("No repository with that QName found in the repositories of that user");
            }else{
                try {
                    // access the desired data source
                    String localPart = url.substring(7); //cut off https:/ (change if git: is used)
                    File file = new File(GitRepositoryDataLoader.localPath + localPart);
                    Helper.cloneRepository(url, temp[2], temp[3], file);
                    // access the files in the data resource and create InputStreams
                    File desiredResource = new File(file.getPath());
                    List<String> paths = Helper.listLocalPaths(desiredResource);
                    return Helper.createInputStreams(paths, desiredResource);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new Exception("Cloning of the repository failed");
                }
            }
        }
    }

    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().equals("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");

        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            try {
                // create client for Github
                GitHubClient client = GitHubClient.createClient("https://github.com/");
                client.setCredentials(temp[2], temp[3]);
                // delete the repository
                String[] localParts = temp[1].split("/");
                client.delete("/repos/" + client.getUser() + "/" + localParts[localParts.length-1]);
            } catch (Exception e) {
                throw new Exception("Deleting of the repository failed or repository not existing");
            }
        }
    }
    
    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }
    
}
