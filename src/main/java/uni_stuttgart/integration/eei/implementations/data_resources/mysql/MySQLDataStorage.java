package uni_stuttgart.integration.eei.implementations.data_resources.mysql;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.PullImageResultCallback;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataStorage;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of MySql instances contained in 
 * docker mysql containers as data source.
 * 
 * @author wederbn
 *
 */
@Service
public class MySQLDataStorage implements IDataStorage{
    
    // the namespace this class is used for
    private String supportedNamespace;
    
    public MySQLDataStorage() throws URISyntaxException{
        // read namespace prefix and create QName with that prefix and the mysql uri
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = provider.getPropertyString("definitions.namespace");
        context.close();
    }

    public String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (!type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Storing data needs a RunnableContainer of an empty resource as input");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            // create docker client related to this empty resource
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            
            // pull the needed Image if not already done
            dockerClient.pullImageCmd("mysql:latest").exec(new PullImageResultCallback()).awaitSuccess();
            
            // load meta-data for the new mysql instance through attributes of the NodeTemplate; use default values if no data is passed
            String dbName = Helper.createRandomName(8); 
            String username = "admin"; 
            String password = "password";
            TEntityTemplate template = resourceEngager.getTargetModel();
            Properties properties = template.getProperties();
            if(properties != null){
                Element element = (Element) properties.getAny();
                if(element.hasAttribute("dbName")){
                    dbName = element.getAttribute("dbName");
                }
                if(element.hasAttribute("userName")){
                    username = element.getAttribute("userName");
                }
                if(element.hasAttribute("password")){
                    password = element.getAttribute("password");
                }
            }
            
            // search for right database image to create empty mediawiki or previously stored version
            String[] namespaceParts = temp[1].split("/");
            String imageName = namespaceParts[namespaceParts.length-2]; //get image name
            if(imageName.equals("newMySql")){
                imageName = "";
            }else{
                imageName = ":" + imageName;
            }

            // define the port mapping to access the instance later
            ExposedPort tcpPort = ExposedPort.tcp(3306);
            Ports portBindings = new Ports();
            portBindings.bind(tcpPort, Ports.binding(null)); // map to random port
            
            // create the containerCmd with the needed environment variables
            CreateContainerResponse container = dockerClient.createContainerCmd("mysql" + imageName)
                    .withExposedPorts(tcpPort)
                    .withPortBindings(portBindings)
                    .withName(dbName)
                    .withHostName("mysql")
                    .withEnv("MYSQL_ROOT_PASSWORD=password", "MYSQL_DATABASE=" + dbName, "MYSQL_USER=" + username, "MYSQL_PASSWORD=" + password)
                    .exec();

            // start the container
            dockerClient.startContainerCmd(container.getId()).exec();
            
            String port = Helper.findPort(temp[2], temp[3], dbName, 3306);
            String dockerIP = temp[2].substring(6).split(":")[0]; // cut out ip address
            
            String uri = "http://" + dockerIP + ":" + port;
            return uri + ";username: " + username + ", password: " + password;
            
//            // wait until container is completely started
//            Thread.sleep(10000); 
//          
//            // create connection to the database
//            try{
//                Class.forName("com.mysql.jdbc.Driver");
//                String port = Helper.findPort(temp[2], temp[3], dbName, 3306);
//                String dockerIP = temp[2].substring(6).split(":")[0]; // cut out ip address
//                String url = "jdbc:mysql://" + dockerIP + ":" + port + "/" + dbName;
//                Connection connection = DriverManager.getConnection(url, username, password);
//                
//                // parse InputStream into MySqlTableObjects
//                List<MySqlTable> tables = new ArrayList<MySqlTable>();
//                for(FileObject currentFile : files){
//                    MySqlTable table = new MySqlTable(currentFile.getName());
//                    String stream = IOUtils.toString(currentFile.getStream(), "UTF-8");
//                    String[] tableParts = stream.split(";");
//                    if(tableParts.length < 4){
//                        throw new Exception("An InputStream is not valid.");
//                    }
//                    table.setColumnNames(tableParts[0].split(","));
//                    table.setColumnTypes(tableParts[1].split(","));
//                    table.setPrimaryKeys(tableParts[2].split(","));
//                    if(!tableParts[3].equals(" ")){
//                        table.setForeignKeys(tableParts[3].split(","));
//                    }
//                    String[] rows = new String[tableParts.length-4];
//                    for(int i = 4; i < tableParts.length; i++){
//                        rows[i-4] = tableParts[i];
//                    }
//                    table.setRows(rows);
//                    tables.add(table);
//                }
//                
//                DatabaseMetaData md = connection.getMetaData();
//                // create tables in the database
//                while(!tables.isEmpty()){
//                    for(int i = 0; i < tables.size(); i++){
//                        MySqlTable table = tables.get(i);
//                        // check if tables of foreign keys are already existing (they have to be created first)
//                        if(table.getForeignKeys() == null || foreignKeyTablesCreated(md, table.getForeignKeys())){
//                            Statement stmt = connection.createStatement();
//                            String query = "CREATE TABLE " + table.getName() + "(";
//                            
//                            // prepare placeholder string for pre-compiling the insert query
//                            String insertValues = "";
//                            
//                            // add all attribute names and types to the query
//                            for(int j = 0; j < table.getColumnNames().length; j++){
//                                query += table.getColumnNames()[j] + " ";
//                                query += table.getColumnTypes()[j] + ",";
//                                
//                                if(j != 0){
//                                    insertValues += ",";
//                                }
//                                insertValues += "?";
//                            }
//                            
//                            // add primary keys
//                            if(table.getPrimaryKeys().length > 0){
//                                query += " primary key(";
//                                for(int j = 0; j < table.getPrimaryKeys().length; j++){
//                                    if(j != 0){
//                                        query += ",";
//                                    }
//                                    query += table.getPrimaryKeys()[j];
//                                }
//                                query += ")";
//                            }
//                            
//                            // add foreign keys
//                            if(table.getForeignKeys() != null){
//                                for(int j = 0; j < table.getForeignKeys().length; j++){
//                                    query += " ,";
//                                    String foreignKeyDefinition = table.getForeignKeys()[j];
//                                    String[] parts = foreignKeyDefinition.split("->");
//                                    query += " FOREIGN KEY (" + parts[0].split("\\.")[1] + ")";
//                                    String[] foreignParts = parts[1].split("\\.");
//                                    query += " REFERENCES " + foreignParts[0] + "(" + foreignParts[1] + ")";
//                                }  
//                            }
//                            
//                            query += ")";
//                            System.out.println(query);
//                            stmt.executeUpdate(query);
//                            
//                            // insert rows
//                            String[] rows = table.getRows();
//                            // prepare query
//                            String insertQuery = "INSERT INTO " + table.getName() + " VALUES (" + insertValues + ")";
//                            PreparedStatement preStmt = connection.prepareStatement(insertQuery);
//                            for(String row : rows){
//                                String[] values = row.split(",");
//                                // ignore possible invalid rows
//                                if(values.length == table.getColumnNames().length){
//                                    // insert values for the placeholder
//                                    for(int j = 1; j <= table.getColumnNames().length; j++){
//                                        preStmt.setString(j, values[j-1]); 
//                                    }
//                                    
//                                    // execute the update 
//                                    preStmt.execute();
//                                }                               
//                            }
//                            
//                            // remove table after creation and restart iteration
//                            tables.remove(i);
//                            break;
//                        }
//                    }
//                }
//            }catch(Exception e){
//               throw new Exception("Unable to load the desired data from the input ressource. Error: " + e.getMessage()); 
//            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        try {
            // images are just supported by acquire and instances just by store and release
            if(operation.equals("acquire")){
                return new URI(this.supportedNamespace + "resources/data-resources/mysql"); 
            }else{
                return new URI(this.supportedNamespace + "resource-instances/data-resources/mysql");
            }    
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
    
//    /**
//     * Check if all tables that are connected with a foreign key are already created
//     */
//    private boolean foreignKeyTablesCreated(DatabaseMetaData md, String[] foreignKeys) throws Exception{
//        for(String table: foreignKeys){
//            // cut remote table out of: this_table.this_column->remote_table.remote_column
//            String remotePart = table.split("->")[1];
//            String remoteTable = remotePart.split("\\.")[0];
//            ResultSet rs = md.getTables(null, null, remoteTable, null);
//            if (!rs.next()) {
//              return false; 
//            }
//        }
//        return true; 
//    }
}
