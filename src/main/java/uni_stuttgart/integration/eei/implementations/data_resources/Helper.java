package uni_stuttgart.integration.eei.implementations.data_resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import java.util.regex.Pattern;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import uni_stuttgart.integration.eei.FileObject;

/**
 * Class to make functions available that are used by implementations for
 * different data sources.
 *
 * @author wederbn
 */

public class Helper {

    /**
     * Create a random String.
     *
     * @param digits The length of the result String.
     * @return A random generated String with the specified length.
     *         {@inheritDoc}
     */
    public static String createRandomName(int digits) {
        // insert all valid characters for the name
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        // creates a string with the valid characters and the length of digits
        for (int i = 0; i < digits; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    /**
     * Create a list of FileObjects containing an InputStream for a file and the
     * local path of the file.
     *
     * @param paths List of paths that shall be converted into FileObjects
     * @param resource The source file for the computation of the local paths
     * @return
     * @throws IOException {@inheritDoc}
     */
    public static List<FileObject> createInputStreams(List<String> paths, File resource) throws IOException {
        // create the returned list
        List<FileObject> dataList = new ArrayList<FileObject>();

        for (String path : paths) {
            // estimate local part and file name
            String localPath = path.substring(resource.getAbsolutePath().length() + 1);
            String[] pathParts = localPath.split(Pattern.quote(File.separator));
            localPath = "";
            for (int i = 0; i < (pathParts.length - 1); i++) {
                localPath += pathParts[i] + "/";
            }
            // add an FileObject for every file
            dataList.add(new FileObject(pathParts[pathParts.length - 1], localPath, new File(path).length(), new FileInputStream(new File(path))));
        }

        return dataList;
    }

    /**
     * Clone remote repository or checkout if already cloned
     *
     * @param url The URL of the repository
     * @param username A valid username to access the repository
     * @param password The valid password for the username
     * @param file Location to which the repository shall be cloned
     * @throws GitAPIException
     * @throws TransportException
     * @throws InvalidRemoteException
     * @throws IOException
     */
    public static void cloneRepository(String url, String username, String password, File file) throws InvalidRemoteException, TransportException, GitAPIException, IOException {
        // repository already cloned --> checkout and pull
        if (file.exists()) {
            FileRepositoryBuilder builder = new FileRepositoryBuilder();
            // search local repository
            Repository repository = builder.setGitDir(new File(file.getAbsolutePath() + File.separator + ".git")).readEnvironment().findGitDir().build();
            Git git = new Git(repository);

            // perform checkout and pull
            git.checkout().setCreateBranch(false).setName(Constants.MASTER).call();
            git.pull().setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password)).call();
            git.close();
        } else {
            // clone repository
            Git.cloneRepository().setURI(url).setDirectory(file).setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password)).call();
        }
    }

    /**
     * List the local paths of all files contained in the source file.
     *
     * @param resource The source file for the listing.
     * @return The list containing all local paths.
     */
    public static List<String> listLocalPaths(File resource) {
        // create the returned list
        List<String> paths = new ArrayList<String>();
        // create stack for searching all files in the resource and add resource location as starting point
        Stack<File> stack = new Stack<File>();
        stack.push(resource);
        
        while (stack.size() > 0) {
            for (File file : stack.pop().listFiles()) {
                // read local path
                String path = file.getAbsolutePath();
                // ignore .git files if resource is a repository
                if (!path.startsWith(resource.getAbsolutePath() + File.separator + ".git")) {
                    if (file.isDirectory()) {
                        // continue search if file is directory
                        stack.push(file);
                    } else {
                        paths.add(path);
                    }
                }
            }
        }
        return paths;
    }
    
    /**
     * Creates all listed files and the needed directories.
     *
     * @param startFile The source directory which shall contain all the files.
     * @param fileList The files that have to be created.
     * @return List of paths of the created files.
     * @throws IOException
     */
    public static List<String> createFiles(File startFile, List<FileObject> fileList) throws IOException {
        
        List<String> filePathList = new ArrayList<String>();
        for (FileObject currentFile : fileList) {
            // read path of the current file and create all needed directories
            File targetFile = new File(startFile.getAbsolutePath() + File.separator + currentFile.getLocalPath() + currentFile.getName());
            targetFile.getParentFile().mkdirs();
            targetFile.getParentFile().setWritable(true);
            
            // write the InputStream to a new file
            OutputStream outStream = new FileOutputStream(targetFile);
            InputStream inStream = currentFile.getStream();

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inStream.read(bytes)) != -1) {
                outStream.write(bytes, 0, read);
            }
            inStream.close();
            outStream.close();
            
            // add file to list
            String localPath = targetFile.getAbsolutePath().replace(File.separatorChar, '/');
            filePathList.add(localPath);
        }
        return filePathList;
    }

    /**
     * Delete the specified directory.
     *
     * @param file The file that contains the path of the repository.
     */
    public static void deleteDirectory(File file) {
        if (file.exists()) {
            // get all files in the current directory
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                // recursively delete files if they are a directory
                if (files[i].isDirectory()) {
                    Helper.deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        // delete directory
        file.delete();
    }
    
    /**
     * Return the port to which a docker container is bound.
     * 
     * @param dockerUri The Uri of the docker client.
     * @param dockerCerthPath The Certh path of the docker client.
     * @param name The name of the container
     * @param searchedPort The inner port of the container
     * @return The outer port to which the specified inner port of the container is bound.
     */
    public static String findPort(String dockerUri, String dockerCerthPath, String name, int searchedPort){
        DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(dockerUri).withDockerCertPath(dockerCerthPath);
        DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
        List<Container> containers = dockerClient.listContainersCmd().exec();
        for (Container container : containers) {
            for (String currentName : container.getNames()) {
                if (currentName.equals("/" + name)) {
                    for (Container.Port port : container.getPorts()) {
                        if (port.getPrivatePort() == searchedPort) {
                            return String.valueOf(port.getPublicPort());
                        }
                    }
                }
            }
        }
        return null;
    }

}
