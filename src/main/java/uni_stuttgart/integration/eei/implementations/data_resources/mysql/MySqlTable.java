package uni_stuttgart.integration.eei.implementations.data_resources.mysql;


/**
 * Represents the data of a table in a MySql database.
 * 
 * @author wederbn
 *
 */
public class MySqlTable {
    String name;
    String[] columnNames = null;
    String[] columnTypes = null;
    String[] primaryKeys = null;
    String[] foreignKeys = null;
    String[] rows = null;
    
    protected MySqlTable(String name){
        this.name = name;
    }
    
    protected String getName(){
        return name;
    }

    protected String[] getColumnNames() {
        return columnNames;
    }

    protected void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }

    protected String[] getColumnTypes() {
        return columnTypes;
    }

    protected void setColumnTypes(String[] columnTypes) {
        this.columnTypes = columnTypes;
    }

    protected String[] getPrimaryKeys() {
        return primaryKeys;
    }

    protected void setPrimaryKeys(String[] primaryKeys) {
        this.primaryKeys = primaryKeys;
    }

    protected String[] getForeignKeys() {
        return foreignKeys;
    }

    protected void setForeignKeys(String[] foreignKeys) {
        this.foreignKeys = foreignKeys;
    }

    protected String[] getRows() {
        return rows;
    }

    protected void setRows(String[] rows) {
        this.rows = rows;
    }

}