package uni_stuttgart.integration.eei.implementations.data_resources.mysql;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of MySql instances contained in 
 * docker mysql containers as data source.
 * 
 * @author wederbn
 *
 */
@Service
public class MySQLDataLoader implements IDataLoader{
    
    // the namespace this class is used for
    private String supportedNamespace;
    
    public MySQLDataLoader() throws URISyntaxException{
        // read namespace prefix and create QName with that prefix and the mysql uri
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = provider.getPropertyString("definitions.namespace");
        context.close();
    }

    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the loading
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            // create docker client related to the resource
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            
            // create random name for new image tag and overwrite if name is set
            String dbName = Helper.createRandomName(8);
            TEntityTemplate template = resourceEngager.getTargetModel();
            Properties properties = template.getProperties();
            if(properties != null){
                Element element = (Element) properties.getAny();
                if(element.hasAttribute("dbName")){
                    dbName = element.getAttribute("dbName");
                }
            }
            
            // list all container and search for the mysql instance
            List<Container> contList = dockerClient.listContainersCmd().exec();
            String[] localParts = temp[1].split("/");
            for(Container cont : contList){
                // search for the desired container
                if(cont.getNames()[0].substring(1).equals(localParts[localParts.length-1])){
                    //commit image
                    dockerClient.commitCmd(cont.getId()).withRepository("mysql").withTag(dbName).exec();
                    break;
                }
            }
            // no return value needed because images are saved internally 
            return null;
            
//            List<FileObject> data = new ArrayList<FileObject>();
//            
//            // Get needed data for accessing the mysql database from the NodeTemplate or through default values.
//            // The default values are only valid if no attributes were passed for the creation of the database.
//            String dbName = type.getLocalPart(); 
//            String username = "admin"; 
//            String password = "password";
//            TEntityTemplate template = resourceEngager.getTargetModel();
//            Properties properties = template.getProperties();
//            if(properties != null){
//                Element element = (Element) properties.getAny();
//                if(element.hasAttribute("userName")){
//                    username = element.getAttribute("userName");
//                }
//                if(element.hasAttribute("password")){
//                    password = element.getAttribute("password");
//                }
//            }
//            
//            // load the ip address from the resource engager data
//            String dockerIP = temp[2].substring(6).split(":")[0];
//            
//            // find the exposed port of the mysql container
//            String mySqlPort = Helper.findPort(temp[2], temp[3], temp[1], 3306); 
//            if(mySqlPort == null){
//                throw new Exception("Exposed port of the docker container was not found.");
//            }
//
//            // create connection to the database
//            Class.forName("com.mysql.jdbc.Driver");
//            String url = "jdbc:mysql://" + dockerIP + ":" + mySqlPort + "/" + dbName;
//            Connection connection = DriverManager.getConnection(url, username, password);
//            
//            // list all tables contained in the addressed database
//            DatabaseMetaData md = connection.getMetaData();
//            ResultSet rs = md.getTables(null, null, "%", null);
//            while (rs.next()) {
//                String currentTableName = rs.getString("TABLE_NAME");
//                String currentTableData = "";
//                
//                // create simple query to get the meta data
//                Statement stmt = connection.createStatement();
//                String query = "Select * from " + currentTableName;
//                ResultSet rsQuery = stmt.executeQuery(query); 
//                ResultSetMetaData resultMetaData = rsQuery.getMetaData();
//
//                // get column names
//                for(int i = 1; i <= resultMetaData.getColumnCount(); i++) {
//                    if(i != 1){
//                        currentTableData += ",";  
//                    }
//                    currentTableData += resultMetaData.getColumnName(i);
//                }
//                currentTableData += ";";
//
//                // get column types
//                for(int i = 1; i <= resultMetaData.getColumnCount(); i++) {
//                    if(i != 1){
//                        currentTableData += ",";  
//                    }
//                    // add column name and precision
//                    String[] typeParts = resultMetaData.getColumnTypeName(i).split(" ");
//                    currentTableData += typeParts[0] + "(" + resultMetaData.getPrecision(i) + ")";
//                    // special case "UNSIGNED" --> precision has to be inserted after the real type
//                    if(typeParts.length > 1){
//                        currentTableData += " " + typeParts[1];
//                    }
//                    // add not null if specified
//                    if (resultMetaData.isNullable(i) == ResultSetMetaData.columnNoNulls){
//                        currentTableData += " NOT NULL";
//                    }
//                    // add auto-increment if specified
//                    if (resultMetaData.isAutoIncrement(i)){ 
//                        currentTableData +=" auto_increment"; 
//                    }
//                }
//                currentTableData += ";";
//                
//                // add primaries to currentTableData
//                ResultSet rsPrimaries = md.getPrimaryKeys(null, null, currentTableName);                
//                while(rsPrimaries.next()){
//                    currentTableData += rsPrimaries.getString("COLUMN_NAME");
//                    if(!rsPrimaries.isLast()){
//                        currentTableData += ",";
//                    }
//                }
//                currentTableData += ";";
//                
//                // add foreign keys to currentTableData
//                ResultSet rsForeignKeys = md.getImportedKeys(connection.getCatalog(), null, currentTableName);
//                while (rsForeignKeys.next()) {
//                    String fkTableName = rsForeignKeys.getString("FKTABLE_NAME");
//                    String fkColumnName = rsForeignKeys.getString("FKCOLUMN_NAME");
//                    String pkTableName = rsForeignKeys.getString("PKTABLE_NAME");
//                    String pkColumnName = rsForeignKeys.getString("PKCOLUMN_NAME");
//                    currentTableData += fkTableName + "." + fkColumnName + "->" + pkTableName + "." + pkColumnName;
//                    if(!rsForeignKeys.isLast()){
//                        currentTableData += ",";
//                    }
//                }
//                currentTableData += " ;";
//                
//                // add rows of the table to currentTableData
//                stmt = connection.createStatement();
//                ResultSet rsRows = stmt.executeQuery("SELECT * FROM " + currentTableName);
//                while (rsRows.next()) {
//                    for(int i = 1; i <= resultMetaData.getColumnCount(); i++){
//                        if(i!=1){
//                            currentTableData += ",";
//                        }
//                        currentTableData += rsRows.getString(i); 
//                    }
//                    currentTableData += ";";
//                }
//                
//                // convert data to InputStream and add stream to the file list
//                InputStream stream = IOUtils.toInputStream(currentTableData, "UTF-8");
//                data.add(new FileObject(currentTableName, null, stream));
//            }  
//            return data; 
        }      
    }

    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {
            // create docker client related to this resource
            DockerClientConfig.DockerClientConfigBuilder builderD = DockerClientConfig.createDefaultConfigBuilder().withDockerHost(temp[2]).withDockerCertPath(temp[3]);
            DockerClient dockerClient = DockerClientBuilder.getInstance(builderD).build();
            
            // iterate over all containers in the docker client
            List<Container> containers = dockerClient.listContainersCmd().exec();
            String[] localParts = temp[1].split("/");
            for(Container container : containers){
                // stop and remove the container that contains the resource
                if(container.getNames()[0].substring(1).equals(localParts[localParts.length-1])){
                    dockerClient.stopContainerCmd(container.getId()).exec();
                    dockerClient.removeContainerCmd(container.getId()).exec();
                }
            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        try {
            // images are just supported by acquire and instances just by store and release
            if(operation.equals("acquire")){
                return new URI(this.supportedNamespace + "resources/data-resources/mysql"); 
            }else{
                return new URI(this.supportedNamespace + "resource-instances/data-resources/mysql");
            }    
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
}
