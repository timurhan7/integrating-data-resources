package uni_stuttgart.integration.eei.implementations.data_resources.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Class to delete a temporary file after closing the input stream.
 * Source: http://stackoverflow.com/questions/4693968/is-there-an-existing-fileinputstream-delete-on-close
 */
public class DeleteOnCloseFileInputStream extends FileInputStream {
    private File file;
    public DeleteOnCloseFileInputStream(String fileName) throws FileNotFoundException{
       this(new File(fileName));
    }
    public DeleteOnCloseFileInputStream(File file) throws FileNotFoundException{
       super(file);
       this.file = file;
    }

    public void close() throws IOException {
        try {
           super.close();
        } finally {
           if(file != null) {
              file.delete();
              file = null;
          }
        }
    }
 }
