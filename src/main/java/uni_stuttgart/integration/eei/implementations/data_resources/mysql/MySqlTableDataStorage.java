package uni_stuttgart.integration.eei.implementations.data_resources.mysql;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataStorage;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for creation of a new empty MySql table instance 
 * for in an existing MySql database.
 * 
 * @author wederbn
 *
 */
@Service
public class MySqlTableDataStorage implements IDataStorage{
    
    // the namespace this class is used for
    private URI supportedNamespace;
    
    public MySqlTableDataStorage() throws URISyntaxException{
        // read namespace prefix and create QName with that prefix and the mysql uri
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/tables/mysql");
        context.close();
    }

    public String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (!type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Storing data needs a RunnableContainer of an empty resource as input");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");
        
        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {            
            // load meta-data for the new table instance through attributes of the NodeTemplate
            String dbName, username, password, tableName; 
            String[] listOfColumnNames, listOfColumnTypes, listOfPrimaryKeys;
            TEntityTemplate template = resourceEngager.getTargetModel();
            Properties properties = template.getProperties();
            if(properties != null){
                Element element = (Element) properties.getAny();
                if(element.hasAttribute("dbName") 
                        && element.hasAttribute("userName") 
                        && element.hasAttribute("password")
                        && element.hasAttribute("tableName")
                        && element.hasAttribute("listOfColumnNames")
                        && element.hasAttribute("listOfColumnTypes")
                        && element.hasAttribute("listOfPrimaryKeys")){
                    dbName = element.getAttribute("dbName");
                    username = element.getAttribute("userName");
                    password = element.getAttribute("password");
                    tableName = element.getAttribute("tableName");
                    listOfColumnNames = element.getAttribute("listOfColumnNames").split(",");
                    listOfColumnTypes = element.getAttribute("listOfColumnTypes").split(",");
                    listOfPrimaryKeys = element.getAttribute("listOfPrimaryKeys").split(",");
                }else{
                    throw new Exception("All attributes need to be set to create a new table!");
                }
            }else{
                throw new Exception("To create a new table the attributes of the NodeTemplate need to be set!");
            }
            
            // check consistency of the attributes
            if(listOfColumnNames.length != listOfColumnTypes.length){
                throw new Exception("The number of column names and column types needs to be the same");
            }
            if(listOfPrimaryKeys.length == 0){
                throw new Exception("The table needs at least one primary key");
            }
 
            try{
                // create the connection to the database
                Class.forName("com.mysql.jdbc.Driver");
                String port = Helper.findPort(temp[2], temp[3], dbName, 3306);
                String dockerIP = temp[2].substring(6).split(":")[0]; // cut out ip address
                String url = "jdbc:mysql://" + dockerIP + ":" + port + "/" + dbName;
                Connection connection = DriverManager.getConnection(url, username, password);
                
                // prepare the query
                Statement stmt = connection.createStatement();
                String query = "CREATE TABLE " + tableName + "(";
                
                // add column names and types to the query
                for(int i = 0; i < listOfColumnNames.length; i++){
                    query += listOfColumnNames[i] + " ";
                    query += listOfColumnTypes[i] + ", ";
                }
                
                // add primary keys to the query
                query += " primary key(";
                boolean first = true;
                for(String pk : listOfPrimaryKeys){
                    if(!first){
                        query += ", ";
                    }
                    query += pk;
                    first = false;
                }
                
                // create the table
                query += "))";
                stmt.executeUpdate(query);
            
                return "http://" + dockerIP + ":" + port + "/" + dbName + "/" + tableName;
            }catch(Exception e){
                throw new Exception("Unable to create the new table. Error: " + e.getMessage()); 
             }
        }
    }

    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }

}
