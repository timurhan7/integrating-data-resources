package uni_stuttgart.integration.eei.implementations.data_resources.mysql;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataLoader;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for loading and releasing of MySql table instances. 
 * Currently the methods are not implemented because just empty
 * table resources are listed in the domain manager. The methods
 * can be implemented if this is changed
 * 
 * @author wederbn
 *
 */
@Service
public class MySqlTableDataLoader implements IDataLoader{
    // the namespace this class is used for
    private URI supportedNamespace;
    
    public MySqlTableDataLoader() throws URISyntaxException{
        // read namespace prefix and create QName with that prefix and the mysql uri
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/tables/mysql");
        context.close();
    }

    public List<FileObject> getData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Loading data from empty resource is not possible");
        }
        
        // implement method if loading of table tata is needed later
        throw new Exception("Loading of data from MySql tables currently not supported");
    }

    public void releaseData(QName type, RunnableContainer resourceEngager) throws Exception {
        // check if desired resource is empty resource and abort the release
        if (type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Releasing data from empty resource is not possible");
        }
        
        // implement method if releasing of tables is needed later
        throw new Exception("Releasing of MySql tables currently not supported");
    }

    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }
}
