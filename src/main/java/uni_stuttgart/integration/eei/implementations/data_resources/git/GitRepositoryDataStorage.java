package uni_stuttgart.integration.eei.implementations.data_resources.git;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.io.IOUtils;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import uni_stuttgart.integration.configuration_files.IConfigurationProvider;
import uni_stuttgart.integration.eei.FileObject;
import uni_stuttgart.integration.eei.IDataStorage;
import uni_stuttgart.integration.eei.implementations.data_resources.Helper;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * Class for usage of git repositories as data source
 *
 * @author wederbn
 *
 */

@Service
public class GitRepositoryDataStorage implements IDataStorage {
    
    // the namespace this class is used for
    private URI supportedNamespace;
    // local path for cloning repositories
    private static final String localPath = "./Repos/";
    
    
    /**
     * Create a GitRepositoryDataStorage and load the namespace prefix of the
     * application
     *
     * @throws URISyntaxException Exception is thrown if namespace in the
     *             properties file is no valid {@link URI}
     */
    public GitRepositoryDataStorage() throws URISyntaxException {
        // read namespace prefix and create QName with that prefix and git
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("uni_stuttgart.integration.configuration_files");
        context.refresh();
        IConfigurationProvider provider = context.getBean(IConfigurationProvider.class);
        this.supportedNamespace = new URI(provider.getPropertyString("definitions.namespace") + "resources/data-resources/git/repository");
        context.close();
    }
    
    public String storeData(QName type, RunnableContainer resourceEngager, List<FileObject> files) throws Exception {
        // check if desired resource is empty resource and abort if not
        if (!type.getLocalPart().endsWith("Empty Resource")) {
            throw new Exception("Storing data needs a RunnableContainer of an empty resource as input");
        }
        
        // read the needed data from the resourceEngager
        String accessData = IOUtils.toString(resourceEngager.getDeployable(), "UTF-8");
        String[] temp = accessData.split(";");

        // check if all needed data is available
        if (temp.length < 4) {
            throw new Exception("Data from resourceEngager was not valid");
        } else {     
            // get name of the new data resource
            String name;
            TEntityTemplate template = resourceEngager.getTargetModel();
            Properties properties = template.getProperties();
            if(properties != null){
                Element element = (Element) properties.getAny();
                if(element.hasAttribute("name")){
                    name = element.getAttribute("name");
                }else{
                    name = Helper.createRandomName(8);  
                }
            }else{
                name = Helper.createRandomName(8);  
            }

            // check if name is already used by the user
            GitHubClient client = new GitHubClient();
            client.setCredentials(temp[2], temp[3]);
            RepositoryService service = new RepositoryService(client);
            for (org.eclipse.egit.github.core.Repository repo : service.getRepositories(temp[2])) {
                // compare local part of the QName of the desired resource with the repo name
                if (repo.getName().equals(name)) {
                    throw new Exception("Name is already in use");
                }
            }
            
            try {
                // create new remote reposiotry at github
                org.eclipse.egit.github.core.Repository repository = new org.eclipse.egit.github.core.Repository();
                repository.setName(name);
                service.createRepository(repository);

                // read url of the repo
                Repository repo = service.getRepository(temp[2], name);
                
                // create local repo and add files
                String repoPath = GitRepositoryDataStorage.localPath + "/" + temp[2] + "/" + name;
                File file = new File(repoPath);
                Git git = Git.init().setDirectory(file).call();
                List<String> newFilePaths = Helper.createFiles(file, files);
                if (newFilePaths == null) {
                    throw new Exception("Name is already in use");
                }
                for (String path : newFilePaths) {
                    path = path.substring(file.getAbsolutePath().length() + 1);
                    git.add().addFilepattern(path).call();
                }
                git.commit().setMessage("Stored data resource: " + name).call();
                
                // push files to the created remote repository
                git = Git.open(file);
                git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(temp[2], temp[3])).setRemote(repo.getCloneUrl()).setForce(true).setPushAll().setPushTags().call();
                git.close();

                // delete files
                Helper.deleteDirectory(file);

                // return URI
                String uri = repo.getCloneUrl().substring(0, repo.getCloneUrl().length() - 4); // cut of .git
                return uri;
            } catch (Exception e) {
                throw new Exception("Creation of the repository failed");
            }
        }
    }

    public URI getSupportedNamespace(String operation) {
        return this.supportedNamespace;
    }
    
}
