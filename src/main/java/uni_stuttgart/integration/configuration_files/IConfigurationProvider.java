package uni_stuttgart.integration.configuration_files;

/**
 * Interface to retrieve properties
 * @author wederbn
 */
public interface IConfigurationProvider {

    /**
     * Return configuration information
     * 
     * @param propertyName Name of the desired property
     * @return List that contains all information saved under that property
     */
    String[] getPropertyStringArray(String propertyName);

    /**
     * Return configuration information
     * 
     * @param propertyName Name of the desired property
     * @return String with the desired property
     */
    String getPropertyString(String propertyName);

}
