package uni_stuttgart.integration.configuration_files;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationProvider implements IConfigurationProvider{

    private FileBasedConfigurationBuilder<FileBasedConfiguration> builder;


    public ConfigurationProvider() {
        Parameters params = new Parameters();
        this.builder = new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class).configure(params.properties().setListDelimiterHandler(new DefaultListDelimiterHandler(',')).setFileName("data.properties"));
    }

    public String[] getPropertyStringArray(String propertyName) {

        try {
            Configuration config = this.builder.getConfiguration();
            return config.getStringArray(propertyName);
        } catch (ConfigurationException e) {
            // loading of the configuration file failed
        }
        return null;
    }

    public String getPropertyString(String propertyName) {
        try {
            Configuration config = this.builder.getConfiguration();
            return config.getString(propertyName);
        } catch (ConfigurationException e) {
            // loading of the configuration file failed
        }
        return null;
    }

}
