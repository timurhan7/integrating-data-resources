package test;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.Test;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.ReleaseInformationResources;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * To perform the test some resource source paths should be set in the data.properties file.
 */
public class ReleaseTest {
    
    @Test
    public void testReleaseWithEmptyRessource() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        ReleaseInformationResources release = new ReleaseInformationResources();
        
        // search an empty resource in the list of resources
        int size = elements.size();
        if(size != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(tempType.getName().endsWith("Empty Resource")){
                        // get available deployables
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        Deployable deployable = agg.getDeployable(elementName, agg.listDeployablesOfResource(elementName).get(0), new TIntention());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        // create RunnableContainer with the deployable and the node template
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
                        
                        release.executeOperation(list, null, new TestOperationCallback());
                        break;
                    } 
                }  
            }
        }
        
        // test if resource count stays constant
        def = agg.listDomain();
        elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        assertEquals(size, elements.size());   
    }
    
    @Test
    public void testReleaseWithExistingRessource() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        ReleaseInformationResources release = new ReleaseInformationResources();
        
        // search an non empty resource in the list of resources
        int size = elements.size();
        boolean objectFound = false;
        if(size != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(!tempType.getName().endsWith("Empty Resource")){
                        // get available deployables
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        Deployable deployable = agg.getDeployable(elementName, agg.listDeployablesOfResource(elementName).get(0), new TIntention());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        // create RunnableContainers with the deployables and the node template
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
                            
                        release.executeOperation(list, null, new TestOperationCallback());
                        objectFound = true;
                        break;
                    }  
                }
            }
        }
        
        // test if resource count decreases if resource to release was found
        def = agg.listDomain();
        elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        if(objectFound || !StatusObject.getStatus()){
            assertEquals(size - 1, elements.size());
        }else{
            // if no resource was found the count has to stay constant
            assertEquals(size, elements.size());  
        }    
    }

}
