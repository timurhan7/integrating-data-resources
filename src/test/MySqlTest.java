package test;

import static org.junit.Assert.assertEquals;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.Test;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.AcquireInformationResources;
import uni_stuttgart.integration.eei.StoreInformationResources;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * To perform the test some MySql resource source paths should be set in the data.properties file.
 */
public class MySqlTest {

    @Test
    public void testAcquireWithMySqlResourceInstance() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        AcquireInformationResources operation = new AcquireInformationResources();
        
        boolean found = false;
        
        // search an empty resource in the list of resources
        int size = elements.size();
        if(size != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(tempType.getTargetNamespace().endsWith("resource-instances/data-resources/mysql")){
                        // get available deployables
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        Deployable deployable = agg.getDeployable(elementName, agg.listDeployablesOfResource(elementName).get(0), new TIntention());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        // create RunnableContainer with the deployable and the node template
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
                        
                        operation.executeOperation(list, null, new TestOperationCallback());
                        found = true;
                        break;
                    } 
                }  
            }
        }
        
        if(found){
            // operation has to end on error with 'resource-instances'
            assertEquals(StatusObject.getStatus(), false);
        }
    }
    
    @Test
    public void testAcquireWithMySqlResource() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        AcquireInformationResources operation = new AcquireInformationResources();
        
        boolean found = false;
        
        // search an empty resource in the list of resources
        int size = elements.size();
        if(size != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(tempType.getTargetNamespace().endsWith("resources/data-resources/mysql")){
                        // get available deployables
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        Deployable deployable = agg.getDeployable(elementName, agg.listDeployablesOfResource(elementName).get(0), new TIntention());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        // create RunnableContainer with the deployable and the node template
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
                        
                        operation.executeOperation(list, null, new TestOperationCallback());
                        found = true;
                        break;
                    } 
                }  
            }
        }
        
        if(found){
            // operation has to end on success with 'resources'
            assertEquals(StatusObject.getStatus(), true);
        }
    }
    
    @Test
    public void testStoreWithMySqlResourceInstance() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        StoreInformationResources operation = new StoreInformationResources();
        
        boolean found = false;
        
        // search an empty resource in the list of resources
        int size = elements.size();
        if(size != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(tempType.getTargetNamespace().endsWith("resource-instances/data-resources/mysql")){
                        // get available deployables
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        Deployable deployable = agg.getDeployable(elementName, agg.listDeployablesOfResource(elementName).get(0), new TIntention());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        // create RunnableContainer with the deployable and the node template
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
                        
                        operation.executeOperation(list, null, new TestOperationCallback());
                        found = true;
                        break;
                    } 
                }  
            }
        }
        
        if(found){
            // operation has to end on success with 'resource-instances'
            assertEquals(StatusObject.getStatus(), true);
        }
    }
    
    @Test
    public void testStoreWithMySqlResource() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        StoreInformationResources operation = new StoreInformationResources();
        
        boolean found = false;
        
        // search an empty resource in the list of resources
        int size = elements.size();
        if(size != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(tempType.getTargetNamespace().endsWith("resources/data-resources/mysql")){
                        // get available deployables
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        Deployable deployable = agg.getDeployable(elementName, agg.listDeployablesOfResource(elementName).get(0), new TIntention());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        // create RunnableContainer with the deployable and the node template
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
                        
                        operation.executeOperation(list, null, new TestOperationCallback());
                        found = true;
                        break;
                    } 
                }  
            }
        }
        
        if(found){
            // operation has to end on error with 'resources'
            assertEquals(StatusObject.getStatus(), false);
        }
    }
}
