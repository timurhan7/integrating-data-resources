package test;

/**
 * Class to decide if a operation was successful or not
 */
public class StatusObject {
    private static boolean status = true;
    
    public static boolean getStatus(){
        return status;
    }
    
    public static void setStatus(boolean newStatus){
        status = newStatus;
    }
}
