package test;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.Test;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.AcquireInformationResources;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

/**
 * To perform the test some resource source paths should be set in the data.properties file.
 */
public class AcquireTest {
    
    @Test
    public void testAcquireWithEmptyRessource() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        AcquireInformationResources operation = new AcquireInformationResources();
        
        if(elements.size() != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(tempType.getName().endsWith("Empty Resource")){
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        for(QName depl : operation.getRequiredDeployableTypes(elementName)){
                            Deployable deployable = agg.getDeployable(elementName, depl, new TIntention());
                            list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));  
                        }
                        operation.executeOperation(list, null, new TestOperationCallback());
                        break;
                    }    
                }
            }
        }      
    }
    
    @Test
    public void testAcquireWithExistingRessource() throws URISyntaxException{
        // load all resources from the DomainManagerAggregator
        DomainManagerAggregator agg = new DomainManagerAggregator();
        Definitions def = agg.listDomain();
        List<TExtensibleElements> elements = def.getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
        AcquireInformationResources operation = new AcquireInformationResources();
        
        if(elements.size() != 0){
            for(TExtensibleElements element : elements){
                // search for resource and ignore relationships
                if(element.getClass().equals(TNodeType.class)){
                    TNodeType tempType = (TNodeType) element;
                    if(!tempType.getName().endsWith("Empty Resource")){
                        QName elementName = new QName(tempType.getTargetNamespace(), tempType.getName());
                        TNodeTemplate template = new TNodeTemplate();
                        
                        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
                        for(QName depl : operation.getRequiredDeployableTypes(elementName)){
                            Deployable deployable = agg.getDeployable(elementName, depl, new TIntention());
                            list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));  
                        }
                        operation.executeOperation(list, null, new TestOperationCallback());
                        break;
                    }    
                }
            }
        }      
    }

}
