package test;

import uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback;

/**
 * Callback for testing purposes.
 */
public class TestOperationCallback implements OperationCallback{

    public void onSuccess(Object outputParameters) {
        StatusObject.setStatus(true);
    }

    public void onError(Object outputParameters){
        StatusObject.setStatus(false);
    }

}
