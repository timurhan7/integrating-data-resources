package demo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xerces.dom.DOMImplementationImpl;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType;
import org.oasis_open.docs.tosca.ns._2011._12.TRelationshipTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.AcquireAdminRelationshipOperation;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

public class AdminRelationship {

    public static void main(String[] args) throws IOException, URISyntaxException {
        DomainManagerAggregator temp = new DomainManagerAggregator();
        Definitions definitions = temp.listDomain();
        
        AcquireAdminRelationshipOperation acquireRel = new AcquireAdminRelationshipOperation();
        
        // get Deployables of a resource
        TEntityType entityElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(12);
        QName elementName = new QName(entityElement.getTargetNamespace(), entityElement.getName());
        Deployable deployable = temp.getDeployable(elementName, temp.listDeployablesOfResource(elementName).get(0), new TIntention());
        TRelationshipTemplate template = new TRelationshipTemplate();

        // add attributes to the template if needed
        DOMImplementation impl = DOMImplementationImpl.getDOMImplementation();
        Document doc = impl.createDocument(null, "testDoc", null); 
        Element element = doc.createElement("Element");
        element.setAttribute("userName", "test123");
        element.setAttribute("password", "password123");
        Properties prop = new Properties();
        prop.setAny(element);
        template.setProperties(prop);
        
        // create RunnableContainer with Deployables and EntityTemplate
        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));
        
        acquireRel.executeOperation(list, null, new OperationCallbackObject());
    }
}