package demo;


import java.net.URISyntaxException;

import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;

import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;

public class PrintResourcesAndRelationships {

    public static void main(String[] args) throws URISyntaxException {
        DomainManagerAggregator temp = new DomainManagerAggregator();
        Definitions definitions = temp.listDomain();

        int i = 0;
        for (TExtensibleElements type : definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation()) {
            TEntityType tempType = (TEntityType) type;
            System.out.println(i + ": " + tempType.getTargetNamespace() + "/" + tempType.getName());
            i++;
        }
    }

}
