package demo;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.apache.commons.io.IOUtils;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType;

import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.ipsm.protocols.integration.Import;

public class PrintXMLSchemaForAResource {

    public static void main(String[] args) throws URISyntaxException {
        DomainManagerAggregator temp = new DomainManagerAggregator();
        Definitions definitions = temp.listDomain();
        TEntityType element = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(5);
        
        InputStream xsdFile = null;
        // check if properties definition for the entity exists
        if(element.getPropertiesDefinition() != null){
            // read related namespace
            String xsdNamespace = element.getPropertiesDefinition().getElement().getNamespaceURI();
            for(Import importFile : temp.getImports()){
                // search import with same namespace and use InputStream 
                if(importFile.getNamespace().toString().equals(xsdNamespace)){
                  xsdFile = importFile.getImport();
                  break;
              }
            }
        }
        if(xsdFile == null){
            System.out.println("No properties definition for this file");
        }else{
            try {
                String schema = IOUtils.toString(xsdFile, "UTF-8");
                for(String line : schema.split("<")){
                    if(!line.equals("")){
                        System.out.println("<" + line);
                    } 
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
