package demo;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xerces.dom.DOMImplementationImpl;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.StoreInformationResources;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

public class AcquireStoreResourceWithoutElection {

    public static void main(String[] args) throws URISyntaxException {
        DomainManagerAggregator temp = new DomainManagerAggregator();
        Definitions definitions = temp.listDomain();
        
        //AcquireInformationResources operation = new AcquireInformationResources();
        StoreInformationResources operation = new StoreInformationResources();
        
        // get Deployables of a resource
        TEntityType entityElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(2);
        QName elementName = new QName(entityElement.getTargetNamespace(), entityElement.getName());
        TNodeTemplate template = new TNodeTemplate();
        
        // add attributes to the template if needed
        DOMImplementation impl = DOMImplementationImpl.getDOMImplementation();
        Document doc = impl.createDocument(null, "testDoc", null); 
        Element element = doc.createElement("Element");
//        element.setAttribute("dbName", "pbkohuqc");
//        element.setAttribute("userName", "admin");
//        element.setAttribute("password", "password");
//        element.setAttribute("tableName", "Person");
//        element.setAttribute("listOfColumnNames", "name, id");
//        element.setAttribute("listOfColumnTypes", "VARCHAR(20), INT");
//        element.setAttribute("listOfPrimaryKeys", "id");
        Properties prop = new Properties();
        prop.setAny(element);
        template.setProperties(prop); // uncomment if needed
        
        // create RunnableContainer with Deployables and EntityTemplate
        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
        for(QName depl : operation.getRequiredDeployableTypes(elementName)){
            Deployable deployable = temp.getDeployable(elementName, depl, new TIntention());
            list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));  
        }
        operation.executeOperation(list, null, new OperationCallbackObject());
    }
}
