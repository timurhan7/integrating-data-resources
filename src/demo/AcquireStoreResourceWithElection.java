package demo;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.xerces.dom.DOMImplementationImpl;
import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate.Properties;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.AcquireInformationResources;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

public class AcquireStoreResourceWithElection {

    public static void main(String[] args) throws URISyntaxException {
        DomainManagerAggregator temp = new DomainManagerAggregator();
        Definitions definitions = temp.listDomain();
        
        AcquireInformationResources acquireResource = new AcquireInformationResources();
        //StoreResourceOperationInstance storeResource = new StoreResourceOperationInstance();
        
        // first element --> resource that has to be loaded
        TEntityType loadElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(1);
        QName elementName = new QName(loadElement.getTargetNamespace(), loadElement.getName());
        InputStream loadDeployable = temp.getDeployable(elementName, temp.listDeployablesOfResource(elementName).get(0), new TIntention()).getDeployable();
        TNodeTemplate loadTemplate = new TNodeTemplate();
        
        // add attributes to the first element if needed
        DOMImplementation impl = DOMImplementationImpl.getDOMImplementation();
        Document doc = impl.createDocument(null, "testDoc", null); 
        Element element = doc.createElement("Element");
        element.setAttribute("name", "testtesttest");
        Properties prop = new Properties();
        prop.setAny(element);
        //loadTemplate.setProperties(prop); // uncomment if needed
        
        // create the RunnableContainer for the first resource
        RunnableContainer loadContainer = new RunnableContainerObject(loadTemplate, loadDeployable, new QName("source-resource"));
        
        // second element --> location to store the resource
        TEntityType storeElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(7);
        elementName = new QName(storeElement.getTargetNamespace(), storeElement.getName());
        InputStream storeDeployable = temp.getDeployable(elementName, temp.listDeployablesOfResource(elementName).get(0), new TIntention()).getDeployable();
        TNodeTemplate storeTemplate = new TNodeTemplate();
        
        // add attributes to the first element if needed
        Element element2 = doc.createElement("Element2");
        element2.setAttribute("name", "testtesttest");
        Properties prop2 = new Properties();
        prop2.setAny(element2);
        //storeTemplate.setProperties(prop2); // uncomment if needed
        
        // create the RunnableContainer for the location
        RunnableContainer storeContainer = new RunnableContainerObject(storeTemplate, storeDeployable, new QName("target-resource"));
        
        // store RunnableContainers in a list and invoke the operations
        List<RunnableContainer> contList = new ArrayList<RunnableContainer>();
        contList.add(loadContainer);
        contList.add(storeContainer);
        acquireResource.executeOperation(contList, null, new OperationCallbackObject());
        //storeResource.executeOperation(contList, null, new OperationCallbackObject());
    }

}
