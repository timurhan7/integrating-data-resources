package demo;



import de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage;
import uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback;

/**
 * OperationCallback stub for testing (passed to Operation classes)
 */
public class OperationCallbackObject implements OperationCallback {
    
    public void onSuccess(Object outputParameters) {
        TOperationMessage output = (TOperationMessage) outputParameters;
        System.out.println("Success");
        if(output.getInstanceLocation() != null){
            if(output.getInstanceLocation().equals("")){
                System.out.println("Resource saved as docker image for future use");
            }else{
                System.out.println("Location: " + output.getInstanceLocation()); 
            }
        }
        if(output.getOtherParameters() != null){
            System.out.println("Other informations: " + output.getOtherParameters().getAny());
        }
    }
    
    public void onError(Object outputParameters) {
        TOperationMessage output = (TOperationMessage) outputParameters;
        System.out.println("Error");
        if(output.getErrorDefinition() != null)
            System.out.println("Error definition: " + output.getErrorDefinition());
    }

}
