package demo;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.Definitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityType;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;

import de.uni_stuttgart.iaas.ipsm.v0.TIntention;
import uni_stuttgart.integration.domain_manager.DomainManagerAggregator;
import uni_stuttgart.integration.domain_manager.RunnableContainerObject;
import uni_stuttgart.integration.eei.ReleaseInformationResources;
import uni_stuttgart.ipsm.protocols.integration.Deployable;
import uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer;

public class ReleaseResource {

    public static void main(String[] args) throws URISyntaxException {
        DomainManagerAggregator temp = new DomainManagerAggregator();
        Definitions definitions = temp.listDomain();
        ReleaseInformationResources operation = new ReleaseInformationResources();
        
        // get Deployables of a resource
        TEntityType entityElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(10);
        QName elementName = new QName(entityElement.getTargetNamespace(), entityElement.getName());
        TNodeTemplate template = new TNodeTemplate();
        
        // create RunnableContainer with Deployables and EntityTemplate
        List<RunnableContainer> list = new ArrayList<RunnableContainer>();
        for(QName depl : operation.getRequiredDeployableTypes(elementName)){
            Deployable deployable = temp.getDeployable(elementName, depl, new TIntention());
            list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));  
        }
        
        // execute the operation
        operation.executeOperation(list, null, new OperationCallbackObject());
    }
}
