# integrating-data-resources

A Clojure library designed to generalize the access, deletion and creation of resources contained in different data sources.
As meta-language for the communication between different components, TOSCA (Topology and Orchestration Specification for Cloud Applications) is used.
Information about TOSCA can be found [here](http://docs.oasis-open.org/tosca/TOSCA/v1.0/os/TOSCA-v1.0-os.html).

This readme file is intended to provide a short introduction into the project and its usage. For this purpose, the core functions 
are explained and some simple code examples are included. For the extension of the project please read the contribution part of this readme.
Further information about the project can be found in the related bachelor thesis, that is added to the [doc folder](https://gitlab.com/timur87/integrating-data-resources/tree/master/doc).

## Usage

### Installation:

First install [leiningen](http://leiningen.org/).

Install the [protocols project](https://gitlab.com/timur87/protocols) into local maven repository, e.g., lein(.bat) install in protocols/.

Download [commons-configuration2-2.0-beta2](http://commons.apache.org/proper/commons-configuration/download_configuration.cgi) Jar and install into local maven repository.

The integrating-data-resources jar can now be built with lein install and will be installed to the local maven repository for usage in other projects.

If you are using eclipse install [counterclockwise](http://doc.ccw-ide.org/) (provides leiningen support) and then drag and drop the project into Project Explorer so that the project will be imported.

In case JetBrains: https://cursive-ide.com/ for Leiningen support.

### Utilization:

Currently the project loads some configuration data from a [data.properties](https://gitlab.com/timur87/integrating-data-resources/blob/master/data.properties) file.
Make sure to create such a file with the needed information in the project directory before using the classes of this project.

The usefull classes for the usage in other projects are the [DomainManagerAggregator](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/domain_manager/DomainManagerAggregator.java)
and the operation classes which can be found in the [eei folder](https://gitlab.com/timur87/integrating-data-resources/tree/master/src/main/java/uni_stuttgart/integration/eei).

#### DomainManagerAggregator:

The DomainManagerAggregator is used to list all available resources/relationships and to provide all needed information for 
the execution of operations on this resources/relationships. It provides the following methods to the user:
* listDomain(): This method returns a TOSCA definitions document, which contains every resource found in the different data sources as TOSCA NodeTypes.
In addition relations between resources are inserted as TOSCA RelationsshipTypes. The method can be used to get an overview of the supported 
resources and to choose a resource for execution of an operation on it.
* getDeployable(QName resourceQname, QName deployableType, TIntention intention): This method can be used to get all information that is needed to execute an operation
on a resource/relationship. The information is returned as a [Deployable](https://gitlab.com/timur87/protocols/blob/master/src/main/java/uni_stuttgart/ipsm/protocols/integration/Deployable.java)
and contained in the InputStream which can be accessed through getDeployable(). The parameters of the method are a QName which identifies the desired
resource clearly, a QName that identifies the desired Deployable and a TIntention object which is currently not used. To get the information about a resource/relationship which was returned by 
listDomain(), the TargetNamespace and the Name of it can be used as Namespace URI and local part of the QName. The QName of the deployable can be 
chosen from one of the provided QNames of listDeployablesOfResource().
* listDeployablesOfResource(QName resourceType): This method returns all QNames of deployables that are provided for the desired resource as a list.
* getImports(): For some data sources, additional information from the user is needed to perform an operation. This information is passed through 
attributes of an xml element that is contained in the TOSCA NodeTemplate properties 
(please look at the [TOSCA spec](http://docs.oasis-open.org/tosca/TOSCA/v1.0/os/TOSCA-v1.0-os.html) for further information). To define
which attributes are needed for which data source xsd files are created by the different domain managers and passed to the
DomainManagerAggregator. So if an operation shall be executed on a specific resource and for that resource a propertie is defined in the definitions
document, the user can get the related xsd file, that specifies the attribtues, through the getImports() method.

#### Usage examples for the DomainManagerAggregator:

Some usage examples can also be found in the [demo folder](https://gitlab.com/timur87/integrating-data-resources/tree/master/src/demo).

List all available resources and relationships of the DomainManagerAggregator and print the targetNamespace and the name to the console:

    DomainManagerAggregator temp = new DomainManagerAggregator();
    Definitions definitions = temp.listDomain();
    for (TExtensibleElements type : definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation()) {
            TEntityType tempType = (TEntityType) type;
            System.out.println(tempType.getTargetNamespace() + "/" + tempType.getName());
    }

Get all informations needed to access the first resource/relationship in the received list as InputStream:

    DomainManagerAggregator temp = new DomainManagerAggregator();
    Definitions definitions = temp.listDomain();
    TEntityType firstElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(0);
    QName elementName = new QName(firstElement.getTargetNamespace(), firstElement.getName());
    List<QName> possibleDeployables = temp.listDeployablesOfResource(elementName);
    InputStream stream = temp.getDeployable(elementName, possibleDeployables.get(0), new TIntention()).getDeployable();
    
Create a (simple) RunnableContainer that can be passed into an operation for the first resource/relationship in the Definitions document (attributes
have to be added to the template if needed for the data source):

    TEntityType firstElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(0);
    QName elementName = new QName(firstElement.getTargetNamespace(), firstElement.getName());
    List<QName> possibleDeployables = temp.listDeployablesOfResource(elementName);
    Deloyable depl = temp.getDeployable(elementName, possibleDeployables.get(0), new TIntention());
    TEntityTemplate template = new TNodeTemplate();
    RunnableContainer resourceEngager = new RunnableContainerObject(template, depl.getDeployable(), depl.getType());
    
Get the xsd file as InputStream for a specific resource/relationship:

    DomainManagerAggregator temp = new DomainManagerAggregator();
    Definitions definitions = temp.listDomain();
    TEntityType firstElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(0);

    InputStream xsdFile = null;
    // check if properties definition for the entity exists
    if(firstElement.getPropertiesDefinition() != null){
        // read related namespace
        String xsdNamespace = firstElement.getPropertiesDefinition().getElement().getNamespaceURI();
        for(Import importFile : temp.getImports()){
            // search import with same namespace and use InputStream 
            if(importFile.getNamespace().equals(xsdNamespace)){
                xsdFile = importFile.getImport();
                break;
            }
        }
    }
    
Create a RunnableContainer with two attributes that can be passed into an operation (assumption: the attributes are listed in the related xsd file):
    
    // get the input stream for the runnable container
    TEntityType firstElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(0);
    QName elementName = new QName(firstElement.getTargetNamespace(), firstElement.getName());
    List<QName> possibleDeployables = temp.listDeployablesOfResource(elementName);
    Deloyable depl = temp.getDeployable(elementName, possibleDeployables.get(0), new TIntention());
    
    // create the template and the properties
    DOMImplementation impl = DOMImplementationImpl.getDOMImplementation();
    Document doc = impl.createDocument(null, "testDoc", null); // create temporary xml document
    Element element = doc.createElement("Element");
    element.setAttribute("name", "new name");   // add attributes
    element.setAttribute("importantAttribute", "value of the attribute");
    Properties properties = new Properties();
    properties.setAny(element); 
    TEntityTemplate template = new TNodeTemplate();
    template.setProperties(properties);  // add properties to the EntityTemplate
    
    // create the runnable container with the stream and the template
    RunnableContainer resourceEngager = new RunnableContainerObject(template, depl.getDeployable(), depl.getType());

#### Operation classes:

The different implemented operations can be found in the [eei folder](https://gitlab.com/timur87/integrating-data-resources/tree/master/src/main/java/uni_stuttgart/integration/eei)
of the project. All operations implement the [OperationRealization](https://gitlab.com/timur87/protocols/blob/master/src/main/java/uni_stuttgart/ipsm/protocols/integration/operations/OperationRealization.java) 
interface, which defines the three interesting operations getOperationDefinition(), getRequiredDeployableTypes() and executeOperation(). 

> TOperation getOperationDefinition();

> List\<QName\> getRequiredDeployableTypes(QName resourceType);

> void executeOperation(RunnableContainer resourceOrRelationshipContainer, Object inputParameters, OperationCallback callback);

The executeOperation() method leads to the execution of the operation that the object represents. It takes RunnableContainers as input
which define on which resources or relationships the operation shall be executed. The RunnableContainers can be generated from Deployables
that are delivered by corresponding Domain Managers. The QNames of the needed Deployables/RunnableContainers are provided by the getRequiredDeployableTypes() method.
In addition a [OperationCallback](https://gitlab.com/timur87/protocols/blob/master/src/main/java/uni_stuttgart/ipsm/protocols/integration/operations/OperationCallback.java)
implementation has to be passed to the execute method. One method of this OperationCallback is called after completion of the operation
with the output parameters (onSuccess() if the operation was successful or onError() otherwise). The last parameter is an Object. It contains
additional input parameters. The structure of this object is defined by the getOperationDefinition() method. This method also defines the structure
of the output parameters that are passed to the OperationCallback implementation. Beside this methods, each operation object has 
further methods that specify the supported resources or relationships for the operation. 

Currently, there are four different operations which are described below:

#### [AcquireAdminRelationshipOperation](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/AcquireAdminRelationshipOperation.java):

The AcquireAdminRelationshipOperation can be used to create an administrator account on a MediaWiki instance. Usually, a MediaWiki instance is
created before the execution of this operation by using the AcquireInformationResources operation. The DomainManagerAggregator lists a RelationshipType
for every active MediaWiki instance. By returning the Deployable for the desired MediaWiki instance the needed information for the instance is delivered.
With that information and a RelationshipTemplate, the RunnableContainer for the operation can be build. It is possible to pass the desired username
and password for the administrator account in the attributes of the properties of the RelationshipTemplate.

#### [AcquireInformationResources](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/AcquireInformationResources.java):

The acquire operation is usually called at the beginning of an informal process. It is used to provide an instance of a desired resource to the user.
It needs RunnableContainer(s) of the desired resource that can be generated from corresponding Deployables.
Which Deployables are needed is defined through the getRequiredDeployableTypes() operation. After the acquiring of a resource
the operation returns a TOperationMessage object to the OperationCallback which includes the location of the new resource instance and
possible additional information that is needed to access the instance (usernames, passwords, ...).

#### [ReleaseInformationResources](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/ReleaseInformationResources.java):

The release operation is usually called at the end of an informal process. It is used to delete a previously acquired instance. It also
needs RunnableContainer(s) of the desired resource to process the release. After the termination of the operation a 
TOperationMessage object is passed to the OperationCallback which indicates whether the operation was successful or not.

#### [StoreInformationResources](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/StoreInformationResources.java):

The store operation is usually called at the end of an informal process too. In contrast to acquire and release the operation is
optional in informal processes. It can be used to store new knowledge or data that was obtained/created in the informal process. 
Therefore, the knowledge or data can be used by later processes with the acquire operation. The operation returns the location of the
stored resource and additional information to the OperationCallback.

#### Usage examples for the operation classes:

Some usage examples can also be found in the [demo folder](https://gitlab.com/timur87/integrating-data-resources/tree/master/src/demo).

Execute an operation with RunnableContainers generated from Deployables that are requested by the operation class through getRequiredDeployableTypes().
In this example, the AcquireResourceOperation is called but the different operations can be called in the same way.
There are two sample attributes added. In a real execution this attributes have to match with the attributes provided by
the properties definition xsd file (see example to read that xsd file above):
    
    // create Definitions document and operation class
    DomainManagerAggregator temp = new DomainManagerAggregator();
    Definitions definitions = temp.listDomain();
    AcquireInformationResources operation = new AcquireInformationResources();  // change for other operations
    
    // get EntityType of a resource (at index 0  of the definitions) and corresponding QName
    TEntityType entityElement = (TEntityType) definitions.getServiceTemplateOrNodeTypeOrNodeTypeImplementation().get(0);
    QName elementName = new QName(entityElement.getTargetNamespace(), entityElement.getName());
    TNodeTemplate template = new TNodeTemplate();
        
    // add properties with attributes to the template
    DOMImplementation impl = DOMImplementationImpl.getDOMImplementation();
    Document doc = impl.createDocument(null, "testDoc", null); 
    Element element = doc.createElement("Element");
    element.setAttribute("attribute1", "value1");
    element.setAttribute("attribute2", "value2");
    Properties prop = new Properties();
    prop.setAny(element);
    template.setProperties(prop);
        
    // create RunnableContainer with Deployables and EntityTemplate
    List<RunnableContainer> list = new ArrayList<RunnableContainer>();
    for(QName depl : operation.getRequiredDeployableTypes(elementName)){
        Deployable deployable = temp.getDeployable(elementName, depl, new TIntention());
        // add RunnableContainer with current Deployable and the created template
        list.add(new RunnableContainerObject(template, deployable.getDeployable(), deployable.getType()));  
    }
    
    // execute operation with the created RunnableContainers and a OperationCallback instance
    operation.executeOperation(list, null, new OperationCallbackObject());

### Tests

Please make sure to have some resource sources listed in the data.properties file when launching 
the test cases. Otherwise, the tests succeed automatically because they do not find resources
to run on. Also, beware that the tests perform some operations like deleting or creating resources.
So do not take important data into the properties file for running the tests.

## Project structure

### An overview how the project is structured

```
Project Root/
│
├── doc/
│
├── src/
│   ├── demo/
│   │  
│   ├── main/
│   │    └── java/
│   │         └── uni_stuttgart/
│   │              └── integration/
│   │                   │
│   │                   ├── configuration_files/
│   │                   │
│   │                   ├── domain_manager/
│   │                   │    └── implementations/
│   │                   │         │ 
│   │                   │         ├── data_resources/
│   │                   │         │
│   │                   │         └── knowledge_resources/
│   │                   │
│   │                   ├── eei/
│   │                   │    └── implementations/
│   │                   │         │ 
│   │                   │         ├── data_resources/
│   │                   │         │
│   │                   │         └── knowledge_resources/
│   │                   │
│   │                   └── meta_data_objects/
│   └── test/
│
├── .gitignore
├── .project
├── CHANGELOG.md
├── LICENSE
├── README.md
├── data.properties
└── project.clj
```

### Meaning of the folders

**doc/**

This folder contains all files (except this README.md) for the documentation of the project.

**src/**

All java code files of the project are contained in this folder.

**src/demo/**

The folder contains some demo files which illustrate how the library can be used.

**src/main/java/uni_stuttgart/integration/**

The library code files are stored here.

**src/main/java/uni_stuttgart/integration/configuration_files/**

This folder contains all files that are needed to load configuration data from the data.properties file.

**src/main/java/uni_stuttgart/integration/domain_manager/**

In this folder, all files concerning the domain managers are stored. It contains the DomainManagerAggregator
and all needed files for the parameters and the IDataResourceDomainManager interface, which has to be implemented
to add a new domain manager to the aggregator. Additionally, the folder contains a subfolder for every kind of
resources (i.e. data_resources, knowledge_resources, ...). In this folders, the implementations of the IDataResourceDomainManager
interface for different data sources are stored.

**src/main/java/uni_stuttgart/integration/eei/**

This folder contains all files that are needed to perform operations on resources or relationships. It includes the
operation classes and the interfaces that need to be implemented to extend an operation with a new data source.
The folder contains a subfolder 'Implementations' which includes a subfolder for every kind of resource. In this
subfolders, the implementations of the eei interfaces for different data sources are stored.

**src/main/java/uni_stuttgart/integration/meta_data_objects/**

The files in this folder are used to store the metadata about data sources as java classes.

**src/test/**

This folder contains all junit test cases and needed data structures for them.

## Description of the currently supported resources and relationships

In this section information about the different supported data sources are provided.
It is defined which attributes are needed for which data source to perform operations and what the transmission format looks like.
In addition, it is shown what kind of source information are needed for which data source. 
With source information, the start point for searching resources is meant (for example, a folder to list the subfolders in it
or a Github account to list all contained repositories). The source information has to be added in the 
[data.properties file](https://gitlab.com/timur87/integrating-data-resources/blob/master/data.properties) currently.
The comments in the file and the following descriptions will help to fill that information in.

Currently supported ressources:
- local folders 
- git repositories on github.com 
- folder in a git repository on any git server
- folder in a dropbox account
- MySql server/databases created by a docker client
- tables in a MySql database
- MediaWiki instances

Currently supported relationships:
- Administrator relationship (Mediawiki instances <-> human user)

### Local folders

For local folders, no attributes are needed to perform the currently supported operations.
But if a new folder shall be created (acquire/store) it is possible to set the name of the folder through
a 'name' attribute. If the attribute is not used, the name will be chosen randomly.

As transmission format, the InputStreams of all files are used. Further the name of the files and the local path in the
folder are used to enable the restoration of the complete structure.

As source information, every local folder can be used and then every subfolder of that folder will be listed as a resource and
can be loaded/created/removed. The source information can be added under the 'FileDataResourceDomainManager.sourcePaths' propertie in the
data.properties file. Example (for windows):
> FileDataResourceDomainManager.sourcePaths = C:\Users\Username, C:\Program Files\Java

### Git repositories

For git repositories, it is the same as for local folders. It is possible to set a name as attribute while creating a new repository but it
is not necessary because the name is generated randomly otherwise.

The transmission format is also the same as for local folders.

As source information usernames and related passwords for github.com accounts can be set. If an account is added, every repository listed in that
account is used as a resource. The information needs to be added to the 'GitDataResourceDomainManager.githubUsernameList' and the 
'GitDataResourceDomainManager.githubPasswordList' properties in the data.properties file. Example:
> GitDataResourceDomainManager.githubUsernameList = username1, username2

> GitDataResourceDomainManager.githubPasswordList = password1, password2

### Folders in git repositories

For folders in a git repository, the attributes and the transmission format is the same as for git repositories and local folders.

For the source information, there are two different possibilities. The first possibility is to add a github.com account as described above. 
Then all folders and subfolders in all repositories of this account are listed as resources. The second possibility is to add a single repository
and with that information every folder in the repository can be listed by the DomainManagerAggregator. To add a single repository the https url, 
the username and the password are needed as information. Example:

> GitDataResourceDomainManager.urlList = https://gitlab.com/user/repo

> GitDataResourceDomainManager.repositoryUsernameList = user

> GitDataResourceDomainManager.repositoryPasswordList = password

### Folders in a Dropbox account

For folders in a dropbox account the attributes and the transmission format are the same as for git repositories, git folders and local folders.

As source information, three different pieces of information are needed. The first is the so called 'access token'. It can be generated through the
[Dropbox website](https://www.dropbox.com/). The other pieces of information are the username and the password of the account that shall be added.
Example:

> Dropbox.accessToken = qXPJQq4y8JAAAAAAAAAACwNE3f7Z8Q9lT686lVFmQ9cuQT61nHrbWdpaCE7HuUie

> Dropbox.username = test@test.de

> Dropbox.password = password

### MySql servers

For MySql servers, there are three different attributes that can be set for the creation of a new instance.
'dbName' defines the name of the database on the created server. 'userName' describes the desired username
and 'password' the corresponding password to access the database. The 'dbName' is generated randomly if the attribute
is not set and for the other two attributes default values are used.

The MySql servers are created with [docker](https://www.docker.com/) and stored back as docker images. Hence, there is no other transmission format.

As source information, the data for accessing a docker client is needed. Therefore, two pieces of information are needed.
The path to the certificates of the docker client and the uri of the docker client. The uri consists of 'tcp://', the ip address
and the port of the client. Example:

> Docker.uri = tcp://192.168.99.100:2376

> Docker.certs = /Users/Username/.docker/machine/certs/

### Tables in MySql servers

Tables in MySql databases are a special case. They can be created but there is no possibility to delete or store a single table.
These operations have to be performed on the surrounding MySql server.

There are seven attributes for table resources. The first three are the attributes of the surrounding MySql server/database.
'dbName', 'userName' and 'password' need to have the right values to enable a connection to the database. Otherwise, it is not possible to
create a new table. The remaining four attributes can be used to configure the new table. 'tableName' defines the name of the created table.
'listOfColumnNames' and 'listOfColumnTypes' are defining the structure of the table. Both need to be a comma seperated list and have to have
the same length. The 'listOfPrimaryKeys' attribute defines the primary keys of the new table. At least one primary key has to be defined and
has to be the name of one column.

Tables can be created in existing MySql databases. Therefore, no source information is needed and a resource is listed automatically for
ever created MySql database.

### MediaWiki instances

For MediaWiki instances, there are three different attributs that can be set. 'mediawikiName' defines the name of the
new MediaWiki instance. 'mediawikiAdmin' and 'mediawikiAdminPass' can be set to create an initial administrator account and are
optional. Additional administrator accounts can be created later by using the 'Administrator relationship'.

MediaWiki instances are created with docker and have the same transmission format and source information as MySql servers.

### Administrator relationship

The administrator relationship can be established between a user and a MediaWiki instance. It has two attributes which define the
account information for the newly created administrator: 'userName' and 'password'.

Administrator relationships can only be created for existing MediaWiki instances and because of that no transmission format or
source information is needed.

## Contribution

There are two ways to extend this project. The first is to add new data sources for the existing operations.
The second is to add new operations and needed interfaces for them. Both are descripted shortly in this section.
This library uses Spring as dependency injection library ([Reading about Spring](http://www.vogella.com/tutorials/SpringDependencyInjection/article.html)).
Spring allows a loose coupled delevopment of different implementations of interfaces. To load the implementations they have to be annotated with 
@service and the package where they are placed has to be defined in the data.properties file. 

Example:

> IDataResourceDomainManager.locations = uni_stuttgart.integration.domain_manager.implementations

> IDataLoader.locations = uni_stuttgart.integration.eei.implementations

### Adding a new data source

To add a new data source to the project, different interfaces have to be implemented. The number and kinds of interfaces depend on
the operations that should be available for the new source. Every implementation has to be annotated with @service to enable the
loading of the implementation with Spring. In addition, the implementation has to be stored in the right package or the package has
to be added to the data.properties file (see example above). At least the [IDataResourceDomainManager interface](https://gitlab.com/timur87/protocols/blob/master/src/main/java/uni_stuttgart/ipsm/protocols/integration/operations/RelationshipOperationRealization.java)
has to be implemented for every new data source. It gives the DomainManagerAggregator the possibility to list the available resources
and to return Deployables for the resources that are needed for the operations. The operations need different interfaces which are
listed hereafter.

[AcquireInformationResources](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/AcquireInformationResources.java):

- [IDataLoader interface](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/IDataLoader.java)

- [IDataStorage interface](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/IDataStorage.java)

[ReleaseInformationResources](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/ReleaseInformationResources.java):

- [IDataLoader interface](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/IDataLoader.java)

[StoreInformationResources](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/StoreInformationResources.java):

- [IDataLoader interface](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/IDataLoader.java)

- [IDataStorage interface](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/IDataStorage.java)

[AcquireAdminRelationshipOperation](https://gitlab.com/timur87/integrating-data-resources/blob/master/src/main/java/uni_stuttgart/integration/eei/AcquireAdminRelationshipOperation.java):

- no interfaces needed because there is only one possible resource type

### Adding a new operation

To add a new operation, a new operation class has to be created and added to the [eei folder](https://gitlab.com/timur87/integrating-data-resources/tree/master/src/main/java/uni_stuttgart/integration/eei).
The operation class has to implement the [ResourceOperationRealization interface](https://gitlab.com/timur87/protocols/blob/master/src/main/java/uni_stuttgart/ipsm/protocols/integration/operations/ResourceOperationRealization.java)
if it is an operation on resources or the [RelationshipOperationRealization interface](https://gitlab.com/timur87/protocols/blob/master/src/main/java/uni_stuttgart/ipsm/protocols/integration/operations/RelationshipOperationRealization.java)
if it is an operation on relationships. When new interfaces are required for the operation they can be added to the eei folder too.
Which interfaces have to be implemented to allow the operation on a new resource has to be documented in this file to enable the extension
of the operations. In addition, a new attribute has to be added to the data.properties file for every added interface to enable the specification 
of locations for the implementations of the interfaces.

### Contribution guidelines

- write tests for new components
- code review
- make self-descriptive commit messages
- complete this readme file when adding new files/components and add further documentation to the [doc/](https://gitlab.com/timur87/integrating-data-resources/tree/master/doc) folder if needed

## License

Copyright © 2016 FIXME

Distributed under the Eclipse Public License version 1.0.
